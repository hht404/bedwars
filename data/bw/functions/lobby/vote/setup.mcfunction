# 投票来确认地图

# 载入投票状态
scoreboard players set $voting vote 1
scoreboard players set $countdown_max vote 60

# 重置残留历史
scoreboard players reset @a vote

# 更新字体展示实体
data modify entity @e[limit=1,type=text_display,tag=lobby_display_start_vote] text set value '[{"text":"投票载入中","color":"aqua"}]'

# 更改主地图
schedule function bw:lobby/map/init 1s

# 发送通知
tellraw @a ["",{"text": "[✌] ","color": "aqua","bold": true}, {"selector":"@s","color": "aqua"}, {"text": " 发起了 ","color": "aqua"},{"selector":"@e[tag=map_name,type=marker]","bold": true},{"text": " 地图投票！","color": "aqua"}]
