# 触发投票

# 管理员正在强开游戏
execute if score $force_start temp matches 1 run tellraw @p [{"text": "[✖] ","color": "red","bold": true},{"text": "当前管理员正在强开游戏，因此无法触发投票！","bold": false}]
execute if score $force_start temp matches 1.. run return 1

# 维护模式
execute if score $system_maintenance debug matches 1 run tellraw @p [{"text": "[✖] ","color": "red","bold": true},{"text": "当前处于维护模式，因此无法发起投票！","bold": false}]
execute if score $system_maintenance debug matches 1 run return 1

# 打断地图复制
execute store result score $map_copy temp run scoreboard players get $map_copying temp
execute if score $map_copy temp matches 1.. run function bw:lobby/map/init/clone/reset
execute if score $map_copy mem matches 1.. run return 1

# 打断投票
execute store result score $voting temp run scoreboard players get $voting vote
execute if score $voting temp matches 1..2 run function bw:lobby/vote/cancel
execute if score $voting temp matches 1..2 run return 1

# 没有解锁地图
execute as @e[type=marker,tag=map,tag=special_map] if score @s map = $map_sel map if data entity @s data.map.Lock run tellraw @p [{"text": "[✖] ","color": "red","bold": true},{"text": "前面的区域，以后再来探索吧～","bold": false}]
execute as @e[type=marker,tag=map,tag=special_map] if score @s map = $map_sel map if data entity @s data.map.Lock as @p at @s run function lib:sounds/error
execute as @e[type=marker,tag=map,tag=special_map] if score @s map = $map_sel map if data entity @s data.map.Lock run return 1

# 玩家不足
execute store result score $total_count player_count if entity @a[team=!debug,scores={afk_time=..179}]

execute unless score $total_count player_count matches 2.. run tellraw @p [{"text": "[✖] ","color": "red","bold": true},{"text": "玩家数量不足，无法发动投票！","bold": false}]
execute unless score $total_count player_count matches 2.. run return 1

# 开始投票
function bw:lobby/vote/setup

# 收尾
scoreboard players reset $voting temp
scoreboard players reset $map_copy temp