# 重置投票
bossbar set lobby players
schedule clear bw:lobby/vote/start
schedule clear bw:lobby/map/init
scoreboard players reset $voting vote
scoreboard players reset $countdown vote
scoreboard players reset $countdown_max vote
scoreboard players reset $player_count vote
scoreboard players reset $voted_player vote
scoreboard players reset @a vote
#function bw:lobby/reset_display
