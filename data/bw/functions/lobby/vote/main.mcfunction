# 投票检测主模块

# 倒计时
scoreboard players remove $countdown vote 1

# 同步
execute store result bossbar lobby value run scoreboard players get $countdown vote 

# 显示投票状态
## 统计
scoreboard players set #agree vote 0
scoreboard players set #opposition vote 0
execute as @a[scores={vote=1}] run scoreboard players add #agree vote 1
execute as @a[scores={vote=2}] run scoreboard players add #opposition vote 1
## 更新文本展示实体
data modify entity @e[limit=1,type=text_display,tag=lobby_display_start_vote] text set value '[{"text":"投票中（","color":"aqua"},{"text":"✔","color":"green"},{"score":{"objective": "vote","name":"#agree"}},{"text":" ✘","color":"red"},{"score":{"objective": "vote","name":"#opposition"}},{"text":"）","color":"aqua"}]'

# 投票结束
## 倒计时
execute if score $countdown vote matches 0 run function bw:lobby/vote/end
## 全部活跃玩家都投了票
execute store result score $player_count vote if entity @a[team=!debug,scores={afk_time=..179}]
execute store result score $voted_player vote if entity @a[scores={vote=1..2,afk_time=..179},team=!debug]
execute unless score $voted_player vote matches 0 if score $voted_player vote = $player_count vote run function bw:lobby/vote/end
