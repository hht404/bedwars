# 设定状态
scoreboard players set $voting vote 2

# 同步倒计时
scoreboard players operation $countdown vote = $countdown_max vote 

# 投票计分板
scoreboard players enable @a[team=!debug,scores={afk_time=..179}] vote 
bossbar set lobby name "投票倒计时"
execute store result bossbar lobby max run scoreboard players get $countdown_max vote
execute store result bossbar lobby value run scoreboard players get $countdown_max vote
bossbar set lobby players @a

# 发送提示
tellraw @a[team=!debug,scores={afk_time=..179}] ["",{"text": "[⚠] ","color": "yellow","bold": true}, {"text": "请投票表决是否使用此地图！ ","color": "yellow"},{"text": "[✔]","color": "green","clickEvent": {"action": "run_command","value": "/trigger vote set 1"},"hoverEvent": {"action": "show_text","contents": {"text": "同意选择该地图","color": "green"}}}," ",{"text": "[✘]","color": "red","clickEvent": {"action": "run_command","value": "/trigger vote set 2"},"hoverEvent": {"action": "show_text","contents": {"text": "否定选择该地图","color": "red"}}}]

# 修改字体展示实体
data modify entity @e[limit=1,type=text_display,tag=lobby_display_start_vote] text set value '[{"text":"投票进行中","color":"aqua"}]'