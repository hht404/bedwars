# 克隆指令区域

# 有指令区：放置接口
execute if block 0 49 -25 white_stained_glass run setblock -8 50 -32 minecraft:redstone_block

# 没有指令区：直接覆盖
execute unless block 0 49 -25 white_stained_glass run function bw:lobby/map/clone_command_area_cover