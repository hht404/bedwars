# 地图初始化 #4

# 重置并且移除选择中的地图数据实体
function bw:lobby/map/init/clone/reset
tag @e[tag=map_selected] remove map_selected

# 收尾
kill @e[tag=map_selected]
forceload remove all
forceload add 150 150 0 0
forceload add 1 1 -150 -150

scoreboard players reset $command_area_checking temp
setblock -8 50 -32 air

# 如果是投票而造成地图加载，则现在允许玩家投票并且同步倒计时
execute if score $voting vote matches 1..2 run function bw:lobby/vote/start

# 如果是使用 /function start_game 造成地图加载，则现在启动游戏
execute if score $force_start temp matches 1 run function bw:game/start
scoreboard players reset $force_start temp