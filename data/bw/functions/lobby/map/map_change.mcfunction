# 更新为选中的地图

# 清除区域
fill 11 45 49 -11 67 71 air
kill @e[type=item,tag=!bypass_kill]

# 微观模型加载
fill 0 43 60 0 44 60 air
setblock 0 43 60 minecraft:structure_block[mode=load]{mode:"LOAD",posX:-11,posY:1,posZ:-11,sizeX:23,sizeY:24,sizeZ:23,name:"map_mini:empty"}
execute if score $map_sel map matches 0 run data modify block 0 43 60 name set value "map_mini:random"
execute unless score $map_sel map matches 0 as @e[type=marker,tag=map] if score @s map = $map_sel map if data entity @s data.map.mini_name run data modify block 0 43 60 name set from entity @s data.map.mini_name

setblock 0 44 60 redstone_block
setblock 0 43 60 air

# 更新地图名称
kill @e[type=marker,tag=map_name]
summon marker 0 44 60 {CustomName:'{"text": "名称载入错误"}',Tags:["map_name"]}
execute if score $map_sel map matches 0 run data modify entity @e[type=marker,limit=1,tag=map_name] CustomName set value '"随机地图"'
execute unless score $map_sel map matches 0 as @e[type=marker,tag=map] if score @s map = $map_sel map run data modify entity @e[type=marker,limit=1,tag=map_name] CustomName set from entity @s data.map.name
data modify entity @e[limit=1,type=text_display,tag=lobby_display_mini_map] text set value '[{"text":"当前地图：","bold": false},{"selector":"@e[type=marker,tag=map_name]"}]'