# 地图游玩处实体 NBT 设定

# Tags
data modify entity @s Tags set value ["temp","map_clone_paste"]

# 位置更新（+x y +z）
## 获得边界最大值
execute store result score $map_border temp run data get storage bw:map border.max
## 坐标运算
scoreboard players operation $map_x temp = $map_border temp
scoreboard players operation $map_z temp = $map_border temp
## 更新实体
execute store result entity @s Pos[0] double 1 run scoreboard players get $map_x temp 
data modify entity @s Pos[1] set value 43d
execute store result entity @s Pos[2] double 1 run scoreboard players get $map_z temp 

# 收尾
scoreboard players reset $map_x temp 
scoreboard players reset $map_z temp 
scoreboard players reset $map_border temp 