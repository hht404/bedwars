# 地图分段复制

# 清除缓存区
fill -100 43 140 101 -64 160 air 

# 地图本体复制到缓存区
execute as @e[type=marker,limit=1,tag=map_clone_copy] at @s run clone ~ 43 ~ ~-201 -64 ~ -100 -64 150 masked

# 缓存区到游玩区
execute as @e[type=marker,limit=1,tag=map_clone_paste] at @s run clone -100 43 150 101 -64 150 ~-201 -64 ~ masked

# 移动实体
execute as @e[type=marker,limit=1,tag=map_clone_copy] at @s run tp @s ~ ~ ~-1
data modify entity @e[type=marker,limit=1,tag=map_clone_paste] Pos[2] set from entity @e[type=marker,limit=1,tag=map_clone_copy] Pos[2]
# Loop
scoreboard players remove $map_copying temp 1
## bossbar
execute store result bossbar lobby value run scoreboard players get $map_copying temp
## loop
execute unless score $map_copying temp matches 1.. run function bw:lobby/map/init4
execute if score $map_copying temp matches 1.. run schedule function bw:lobby/map/init/clone/marker_clone 1t replace