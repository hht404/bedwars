# 分段复制地图 初始化
# as /at 地图中心点

# 计算地图长宽
execute as @e[type=marker,tag=map] if score @s map = $map_using map store result score $map_copying temp run data get entity @s data.map.border.max
scoreboard players operation $map_copying temp *= $2 const

# debug ! 
scoreboard players set $map_copying temp 201

# 创造 BossBar 并且设定为复制状态
bossbar set lobby name [{"text": "拷贝中..."}]
execute store result bossbar lobby max run scoreboard players get $map_copying temp
bossbar set lobby players @a

# 生成对应实体
execute summon marker run function bw:lobby/map/init/clone/marker_copy_nbt
execute summon marker run function bw:lobby/map/init/clone/marker_paste_nbt

# 分段复制
schedule function bw:lobby/map/init/clone/marker_clone 2t replace

# 收尾
scoreboard players reset $map_border temp