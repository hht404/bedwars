# 覆盖指令区域

# 检测可用
execute if score $command_area_checking temp matches 1 run scoreboard players set $command_area_working temp 1
execute if score $command_area_checking temp matches 1 run return 1

# 清除区域
fill -11 48 -36 11 96 -14 air 
kill @e[type=item,tag=!bypass_kill]

# 放置结构放宽
setblock 0 48 -25 minecraft:structure_block[mode=load]{mode:"LOAD",posX:-11,posY:1,posZ:-11,sizeX:23,sizeY:48,sizeZ:23,ignoreEntities:false,"name":"command_area:empty"}

# 填入结构名称
execute as @e[type=marker,tag=map] if score @s map = $map_sel map if data entity @s data.map.command_area_name run data modify block 0 48 -25 name set from entity @s data.map.command_area_name

# 召唤结构
setblock 0 47 -25 redstone_block
fill 0 48 -25 0 47 -25 air

# 特殊地图：优先锁定（有指令区域的前提下）
execute as @e[type=marker,tag=map,tag=special_map] if score @s map = $map_sel map if data entity @s data.map.command_area_name run data modify entity @s data.map.Lock set value 1b