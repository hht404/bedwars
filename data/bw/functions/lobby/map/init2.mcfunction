# 地图初始化 #2

# 放置强加强区块
forceload add ~150 ~150 ~ ~ 
forceload add ~1 1 ~-150 ~-150

# 召唤中心点
kill @e[type=marker,tag=map_center]
execute summon marker run function bw:lobby/map/init/map_center_nbt

# 删除forceloader（检测到有中心点的情况下）
execute if entity @e[type=marker,tag=map_center] run kill @s
execute if entity @e[type=marker,tag=map_center] run fill 0 319 0 0 318 0 air

# 由中心点执行后续操作
execute as @e[type=marker,tag=map_center] at @s run function bw:lobby/map/init/clone/setup