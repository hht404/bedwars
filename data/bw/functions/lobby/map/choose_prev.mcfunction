# 选择上一张地图
scoreboard players remove $map_sel map 1
execute if score $map_sel map < $map_min map run scoreboard players operation $map_sel map = $map_max map

# 加载指令区域
scoreboard players reset $command_area_checking temp
function bw:lobby/map/clone_command_area_cover

# 更改微观模型
function bw:lobby/map/map_change

# 解锁地图
## 道具控制
clear @a[team=lobby] snowball{Tags:["disable_throw","map_unlock"],display:{Name:'{"text":"试图解锁"}'}}
execute as @e[type=marker,tag=map,tag=special_map] if score @s map = $map_sel map if data entity @s data.map.command_area_name run item replace entity @a[team=lobby] hotbar.4 with snowball{Tags:["disable_throw","map_unlock"],display: {"Name":'{"text":"试图解锁"}'}} 1

# 打断投票
execute if score $voting vote matches 1..2 run function bw:lobby/vote/cancel

# 打断地图复制
execute if score $map_copying temp matches 1 run function bw:lobby/map/init/clone/reset

# 消息提示
title @a actionbar ["",{"text": "游戏地图 更改 >> ","color": "aqua"},{"selector":"@e[tag=map_name,type=marker]","bold": true}]