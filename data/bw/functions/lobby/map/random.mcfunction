# 随机地图额外操作

# 随机挑选一张地图
scoreboard players set $random_min temp 1
execute store result score $random_max temp run scoreboard players get $map_max map
function lib:random
execute store result score $map_using map run scoreboard players get $random temp

scoreboard players reset $random temp
scoreboard players reset $random_min temp
scoreboard players reset $random_max temp

# 载入指令区域
function bw:lobby/map/clone_command_area_cover