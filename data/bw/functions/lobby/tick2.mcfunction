# 每 2 Tick 执行一次

# 防止想不开
effect give @a[x=-200,y=40,z=-200,dx=400,dy=8,dz=400,team=lobby,gamemode=adventure] minecraft:levitation 1 1
# tp @a[x=-200,y=49,z=-200,dx=400,dy=0,dz=400,team=lobby,gamemode=adventure] 0.5 50.5 0.5 0 0

# 道具调用
function bw:lobby/entertainment/tick2

# 选择上一张地图
execute if block 2 50 36 minecraft:stone_button[face=floor,facing=north,powered=true] run function bw:lobby/map/choose_prev
setblock 2 50 36 minecraft:stone_button[face=floor,facing=north,powered=false]

# 选择下一张地图
execute if block -2 50 36 minecraft:stone_button[face=floor,facing=north,powered=true] run function bw:lobby/map/choose_next
setblock -2 50 36 minecraft:stone_button[face=floor,facing=north,powered=false]

# 发起地图投票
execute positioned 0 50 36 if block ~ ~ ~ minecraft:polished_blackstone_button[face=floor,facing=north,powered=true] as @p run function bw:lobby/vote/trigger
setblock 0 50 36 minecraft:polished_blackstone_button[face=floor,facing=north,powered=false]

# Forceloade （相对强加载区块支持-地图加载时）
execute as @e[type=armor_stand,tag=forceloader] at @s run forceload add ~100 ~100 ~-100 ~-100

# 检测盔甲架是否有传送到地图本体
execute positioned 0 319 0 as @e[type=armor_stand,tag=forceloader,distance=100..] run function bw:lobby/map/init2

# 地图解锁
## 尝试解锁地图
execute as @e[type=snowball,nbt={Item:{tag:{Tags:["map_unlock"]}}}] on origin run tag @s add map_unlocker
execute as @e[type=snowball,nbt={Item:{tag:{Tags:["map_unlock"]}}}] run setblock 8 50 -32 minecraft:redstone_block
## 更新解锁状态
execute unless score $map_sel map matches 0 as @e[type=marker,tag=map] if score @s map = $map_sel map if data entity @s data.map.Lock run data modify entity @e[type=text_display,limit=1,tag=lobby_display_start_vote] text set value '[{"text":"✘ 仍未解锁 ✘","colour":"red"}]'
execute unless score $map_sel map matches 0 as @e[type=marker,tag=map] if score @s map = $map_sel map unless data entity @s data.map.Lock run data modify entity @e[type=text_display,limit=1,tag=lobby_display_start_vote] text set value '[{"text": ">>","bold": true},{"text":" 开始投票 ","bold": false},{"text":"<<","bold": true}]'
execute if score $map_sel map matches 0 run data modify entity @e[type=text_display,limit=1,tag=lobby_display_start_vote] text set value '[{"text": ">>","bold": true},{"text":" 开始投票 ","bold": false},{"text":"<<","bold": true}]'

# 统计信息
## 表现分
execute positioned 32 50 -6 as @a[team=lobby,distance=0..2] run team join stat_score @s
execute positioned 32 50 -6 as @a[team=stat_score,distance=2.1..] run team join lobby @s
## 破床
execute positioned 33 50 -2 as @a[team=lobby,distance=0..2] run team join stat_bed_break @s
execute positioned 33 50 -2 as @a[team=stat_bed_break,distance=2.1..] run team join lobby @s
## 击杀
execute positioned 33 50 2 as @a[team=lobby,distance=0..2] run team join stat_kill @s
execute positioned 33 50 2 as @a[team=stat_kill,distance=2.1..] run team join lobby @s
## 死亡
execute positioned 32 50 6 as @a[team=lobby,distance=0..2] run team join stat_death @s
execute positioned 32 50 6 as @a[team=stat_death,distance=2.1..] run team join lobby @s