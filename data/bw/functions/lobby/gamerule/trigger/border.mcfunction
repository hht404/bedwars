# 边界设定（1=在设定时间后自动缩圈 2=不会自动缩圈）
scoreboard players add $border gamerule 1
execute if score $border gamerule matches 3 run scoreboard players set $border gamerule 1

kill @e[type=marker,tag=gamerule_border]
execute if score $border gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"不会缩圈"}',Tags:["gamerule_border","gamerule"]}
execute if score $border gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"自动缩圈"}',Tags:["gamerule_border","gamerule"]}

data merge block 1 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/border"},"text":"边界设定"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_border]"}','{"text":""}']}}

title @a actionbar [{"text": "游戏规则 更新 >> 边界缩圈："},{"selector":"@e[type=marker,tag=gamerule_border]","bold": true}]