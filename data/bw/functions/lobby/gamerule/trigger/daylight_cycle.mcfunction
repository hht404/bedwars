# 昼夜更替（1=启用 2=关闭）
scoreboard players add $daylight_cycle gamerule 1
execute if score $daylight_cycle gamerule matches 3 run scoreboard players set $daylight_cycle gamerule 1

kill @e[type=marker,tag=gamerule_daylight_cycle]
execute if score $daylight_cycle gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"启用"}',Tags:["gamerule_daylight_cycle","gamerule"]}
execute if score $daylight_cycle gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"停用"}',Tags:["gamerule_daylight_cycle","gamerule"]}

data merge block -1 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/daylight_cycle"},"text":"昼夜更替"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_daylight_cycle]"}','{"text":""}']}}

title @a actionbar [{"text": "游戏规则 更新 >> 昼夜更替："},{"selector":"@e[type=marker,tag=gamerule_daylight_cycle]","bold": true}]