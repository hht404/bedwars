# 兑换规则（1=合成 2=交易）
scoreboard players add $exchange gamerule 1
execute if score $exchange gamerule matches 3 run scoreboard players set $exchange gamerule 1

kill @e[type=marker,tag=gamerule_exchange]
execute if score $exchange gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"合成"}',Tags:["gamerule_exchange","gamerule"]}
execute if score $exchange gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"交易"}',Tags:["gamerule_exchange","gamerule"]}

data merge block -2 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/exchange"},"text":"兑换规则"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_exchange]"}','{"text":""}']}}

title @a actionbar [{"text": "游戏规则 更新 >> 兑换规则："},{"selector":"@e[type=marker,tag=gamerule_exchange]","bold": true}]