# 虚空伤害（0=原版 1=额外添加伤害 2=立即死亡）
scoreboard players add $void_damage gamerule 1
execute if score $void_damage gamerule matches 3 run scoreboard players set $void_damage gamerule 0

kill @e[type=marker,tag=gamerule_void_damage]
execute if score $void_damage gamerule matches 0 run summon marker 0 0 0 {CustomName:'{"text":"原版伤害"}',Tags:["gamerule_void_damage","gamerule"]}
execute if score $void_damage gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"添加伤害"}',Tags:["gamerule_void_damage","gamerule"]}
execute if score $void_damage gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"立即死亡"}',Tags:["gamerule_void_damage","gamerule"  ]}

data merge block 0 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/void_damage"},"text":"虚空伤害"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_void_damage]"}','{"text":""}']}}

title @a actionbar [{"text": "游戏规则 更新 >> 虚空伤害："},{"selector":"@e[type=marker,tag=gamerule_void_damage]","bold": true}]