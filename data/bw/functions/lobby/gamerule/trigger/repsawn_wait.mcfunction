# 复活选项（1=需要等待设定的时间 2=立即复活）
scoreboard players add $respawn_wait gamerule 1
execute if score $respawn_wait gamerule matches 3 run scoreboard players set $respawn_wait gamerule 1

kill @e[type=marker,tag=gamerule_respawn_wait]
execute if score $respawn_wait gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"等待时间"}',Tags:["gamerule_respawn_wait","gamerule"]}
execute if score $respawn_wait gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"立即复活"}',Tags:["gamerule_respawn_wait","gamerule"]}

data merge block 2 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/repsawn_wait"},"text":"复活设定"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_respawn_wait]"}','{"text":""}']}}

title @a actionbar [{"text": "游戏规则 更新 >> 复活选项："},{"selector":"@e[type=marker,tag=gamerule_respawn_wait]","bold": true}]