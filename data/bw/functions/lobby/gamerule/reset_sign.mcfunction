# 重新召唤实体
kill @e[type=marker,tag=gamerule]
## 边界设定
execute if score $border gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"不会缩圈"}',Tags:["gamerule_border","gamerule"]}
execute if score $border gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"自动缩圈"}',Tags:["gamerule_border","gamerule"]}
## 昼夜更替
execute if score $daylight_cycle gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"启用"}',Tags:["gamerule_daylight_cycle","gamerule"]}
execute if score $daylight_cycle gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"停用"}',Tags:["gamerule_daylight_cycle","gamerule"]}
## 兑换规则
execute if score $exchange gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"合成"}',Tags:["gamerule_exchange","gamerule"]}
execute if score $exchange gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"交易"}',Tags:["gamerule_exchange","gamerule"]}
## 复活选项
execute if score $respawn_wait gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"等待时间"}',Tags:["gamerule_respawn_wait","gamerule"]}
execute if score $respawn_wait gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"立即复活"}',Tags:["gamerule_respawn_wait","gamerule"]}
## 虚空伤害
execute if score $void_damage gamerule matches 0 run summon marker 0 0 0 {CustomName:'{"text":"原版伤害"}',Tags:["gamerule_void_damage","gamerule"]}
execute if score $void_damage gamerule matches 1 run summon marker 0 0 0 {CustomName:'{"text":"添加伤害"}',Tags:["gamerule_void_damage","gamerule"]}
execute if score $void_damage gamerule matches 2 run summon marker 0 0 0 {CustomName:'{"text":"立即死亡"}',Tags:["gamerule_void_damage","gamerule"  ]}

# 重置告示牌
data merge block 2 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/repsawn_wait"},"text":"复活设定"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_respawn_wait]"}','{"text":""}']}}
data merge block 1 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/border"},"text":"边界设定"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_border]"}','{"text":""}']}}
data merge block 0 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/void_damage"},"text":"虚空伤害"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_void_damage]"}','{"text":""}']}}
data merge block -1 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/daylight_cycle"},"text":"昼夜更替"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_daylight_cycle]"}','{"text":""}']}}
data merge block -2 51 20 {back_text:{color:"black",has_glowing_text:0b,messages:['{"text":""}','{"text":""}','{"text":""}','{"text":""}']},front_text:{color:"black",has_glowing_text:1b,messages:['{"text":""}','{"clickEvent":{"action":"run_command","value":"function bw:lobby/gamerule/trigger/exchange"},"text":"兑换规则"}','{"selector":"@e[type=minecraft:marker,limit=1,tag=gamerule_exchange]"}','{"text":""}']}}