# 要求玩家载入状态0：大厅

# 游戏数据
gamemode adventure @s
spawnpoint @s 0 50 0 0

# 清理残留数据
scoreboard players reset @s rejoin_trigger
tag @s remove lose_bed

# 清除背包
clear @s

# 队伍
team join lobby @s

# 药水效果
effect clear @s 
effect give @s[scores={health=..19}] regeneration 3 255 true

# 传送
tp @s 0.50 50.00 0.50 0 0

# 如果正在投票则发送投票请求
execute if score $voting vote matches 2 unless score @s vote matches 1..2 run tellraw @s ["",{"text": "[⚠] ","color": "yellow","bold": true}, {"text": "请投票表决是否使用此地图！ ","color": "yellow"},{"text": "[✔]","color": "green","clickEvent": {"action": "run_command","value": "/trigger vote set 1"},"hoverEvent": {"action": "show_text","contents": {"text": "同意选择该地图","color": "green"}}}," ",{"text": "[✘]","color": "red","clickEvent": {"action": "run_command","value": "/trigger vote set 2"},"hoverEvent": {"action": "show_text","contents": {"text": "否定选择该地图","color": "red"}}}]

# 如果正在拷贝地图则展示 Bossbar
execute if score $map_copying temp matches 1.. run bossbar set lobby players @a