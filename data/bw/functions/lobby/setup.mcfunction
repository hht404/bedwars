# 要求系统载入状态0：大厅
scoreboard players set $working gaming 0

# 游戏规则
gamerule spectatorsGenerateChunks false
gamerule fallDamage false
gamerule naturalRegeneration true
gamerule doDaylightCycle true
gamerule doFireTick true

# 世界出生点
setworldspawn 0 50 0 0

# 初始化地图选择
execute store result score $map_max map if entity @e[type=marker,tag=map,tag=!map_importer,tag=!special_map]
execute store result score $map_min map if entity @e[type=marker,tag=map,tag=!map_importer,tag=special_map]
scoreboard players operation $map_min map *= $-1 const

scoreboard players set $map_sel map 0

# 载入指令区域
function bw:lobby/map/clone_command_area

# HUB 控制
scoreboard objectives setdisplay belowName
scoreboard objectives setdisplay list
scoreboard objectives setdisplay sidebar
scoreboard objectives setdisplay sidebar.team.blue
scoreboard objectives setdisplay sidebar.team.green
scoreboard objectives setdisplay sidebar.team.red
scoreboard objectives setdisplay sidebar.team.yellow
scoreboard objectives setdisplay sidebar.team.gray
scoreboard objectives setdisplay sidebar.team.dark_aqua stat_score
scoreboard objectives setdisplay sidebar.team.dark_purple stat_bed_break
scoreboard objectives setdisplay sidebar.team.dark_green stat_kill
scoreboard objectives setdisplay sidebar.team.dark_red stat_death
bossbar set countdown:blue players
bossbar set countdown:green players
bossbar set countdown:yellow players
bossbar set countdown:red players
bossbar set countdown:spec players

# 载入微观模型
function bw:lobby/map/map_change

# 重置展示实体
function bw:lobby/reset_display

# 重置规则牌
function bw:lobby/gamerule/reset_sign

# 玩家载入
execute as @a[team=!debug] run function bw:lobby/player_enter