# 版本检测

# 版本号检测
execute unless data storage temp version.last run data modify storage temp version.last set from storage temp version.current
execute store result score #version_last mem run data get storage temp version.last
execute store result score #version_current mem run data get storage temp version.current
data modify storage temp version.last set from storage temp version.current

# 如果当前版本高于以前版本：升级
execute if score #version_current mem > #version_last mem run function bw:lobby/version/upgrade

# 如果当前版本低于以前版本：降级
execute if score #version_current mem < #version_last mem run function bw:lobby/version/downgrade