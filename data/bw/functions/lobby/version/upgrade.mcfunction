# 升级数据包操作

# 同步配置
function debug:config_reset

# 向玩家发送全局初始化提示
tellraw @a [{"text": "[⚠] ","color": "yellow","bold": true},{"text": "检测到新数据包版本，正在开展全局初始化...","bold": false}]

# 展开初始化
function init