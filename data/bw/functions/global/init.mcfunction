# 全局初始化

# 世界边界
worldborder center 0 0
worldborder set 29999984

# 强加载
forceload remove all
forceload add 85 85 -85 -85 

# 计分板
scoreboard objectives remove debug
scoreboard objectives add debug dummy "调试"
execute unless score $system_f3 debug matches 0..1 run scoreboard players set $system_f3 debug 1
execute unless score $system_maintenance debug matches 0..1 run scoreboard players set $system_maintenance debug 0
execute unless score $team_auto_allocation debug matches 0..1 run scoreboard players set $team_auto_allocation debug 0
execute unless score $rule_res_cd debug matches 0..1 run scoreboard players set $rule_res_cd debug 0


scoreboard objectives remove const
scoreboard objectives add const dummy "数字常数"
scoreboard players set $-1 const -1
scoreboard players set $1 const 1
scoreboard players set $2 const 2
scoreboard players set $4 const 4
scoreboard players set $5 const 5
scoreboard players set $10 const 10
scoreboard players set $1000 const 1000

scoreboard objectives remove temp
scoreboard objectives add temp dummy "缓存"

scoreboard objectives remove food
scoreboard objectives add food food "饱食"

scoreboard objectives remove leave_game
scoreboard objectives add leave_game custom:leave_game "退出游戏"

scoreboard objectives remove afk_time
scoreboard objectives add afk_time dummy "离开时间"

scoreboard objectives remove respawn_time
scoreboard objectives add respawn_time dummy "复活等待时间"

scoreboard objectives remove death
scoreboard objectives add death deathCount "死亡接口"

scoreboard objectives remove kill_player
scoreboard objectives add kill_player playerKillCount "击杀玩家总数"

scoreboard objectives remove health
scoreboard objectives add health health "❤"

scoreboard objectives remove UUID
scoreboard objectives add UUID dummy "统一标识符"

scoreboard objectives remove damage_type
scoreboard objectives add damage_type dummy "伤害种类"

scoreboard objectives remove damage_source
scoreboard objectives add damage_source dummy "伤害来源"

scoreboard objectives remove hate_time
scoreboard objectives add hate_time dummy "仇恨时间"

scoreboard objectives remove gamerule
scoreboard objectives add gamerule dummy "游戏设定"

scoreboard objectives remove fireball_life
scoreboard objectives add fireball_life dummy "火球时间"

scoreboard objectives remove player_count
scoreboard objectives add player_count dummy "玩家人数数据"

scoreboard objectives remove map
scoreboard objectives add map dummy "地图"
execute as @e[type=marker,tag=map] store result score @s map run data get entity @s data.map.id

scoreboard objectives remove shop
scoreboard objectives add shop dummy "商店绑定"
execute as @e[type=marker,tag=shop] store result score @s shop run data get entity @s data.shop.id

scoreboard objectives add config dummy "配置"
execute unless score #init config matches 1 run function debug:config_reset
function debug:config_sync

# 队伍
# team remove debug
team add debug "调试"
team modify debug collisionRule never
team modify debug color light_purple
team modify debug friendlyFire true