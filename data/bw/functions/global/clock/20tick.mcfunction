# 每 20 Tick 执行一次
schedule function bw:global/clock/20tick 20t replace

# 状态执行
execute if score $working gaming matches 0 run function bw:lobby/tick20
execute if score $working gaming matches 1 run function bw:game/tick20

# 暴力的挂机检测
tag @a add afk_check
tag @a[predicate=lib:sneaking] remove afk_check
tag @a[predicate=lib:sprinting] remove afk_check
tag @a[predicate=lib:swimming] remove afk_check
execute as @a unless score @s afk_time matches -1 run scoreboard players set @s[tag=!afk_check] afk_time 0
execute as @a unless score @s afk_time matches -1 run scoreboard players add @s[tag=afk_check] afk_time 1

# 禁止捡起箭（在落地 10 秒后清理）
execute as @e[type=arrow,tag=!projectile_checked] run function bw:global/event/arrow_nbt

# 掐死原版 BGM
# stopsound @a music music.game
# stopsound @a music music.creative
