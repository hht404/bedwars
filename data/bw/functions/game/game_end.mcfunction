# 游戏结束

# HUB 控制
scoreboard objectives setdisplay belowName
scoreboard objectives setdisplay list
scoreboard objectives setdisplay sidebar
scoreboard objectives setdisplay sidebar.team.blue
scoreboard objectives setdisplay sidebar.team.green
scoreboard objectives setdisplay sidebar.team.red
scoreboard objectives setdisplay sidebar.team.yellow
scoreboard objectives setdisplay sidebar.team.gray
bossbar set countdown:blue players
bossbar set countdown:green players
bossbar set countdown:yellow players
bossbar set countdown:red players
bossbar set countdown:spec players

# 获得胜利的玩家
tag @a remove win

function bw:game/team_update
execute if score $blue_number team matches 1 run tag @a[team=blue] add win
execute if score $green_number team matches 1 run tag @a[team=green] add win
execute if score $yellow_number team matches 1 run tag @a[team=yellow] add win
execute if score $red_number team matches 1 run tag @a[team=red] add win

execute unless entity @a[tag=win] run scoreboard players set $no_winner temp 1

# 计算排名
tag @a remove total_rank1
tag @a remove total_rank2
tag @a remove total_rank3
tag @a remove total_rankl

## 1
function bw:game/gane_end_get_max_score
execute as @a if score @s round_score = $score_max temp run tag @s add total_rank1
tag @a[tag=total_rank1] add max_uncounted
## 2
function bw:game/gane_end_get_max_score
execute as @a[tag=!max_uncounted] if score @s round_score = $score_max temp run tag @s add total_rank2
tag @a[tag=total_rank2] add max_uncounted
## 3
function bw:game/gane_end_get_max_score
execute as @a[tag=!max_uncounted] if score @s round_score = $score_max temp run tag @s add total_rank3
tag @a[tag=max_uncounted] remove max_uncounted
## 最低分
scoreboard players set $score_min temp 2147483647
scoreboard players operation $score_min temp < @a[tag=playing] round_score
execute as @a if score @s round_score = $score_min temp run tag @s add total_rankl
## 没有名次
tag @a[tag=playing,tag=!total_rank1,tag=!total_rank2,tag=!total_rank3] add total_rankl

# 显示排名
tellraw @a ["",{"text":"\n------  全场游戏结束！ ------\n","color":"light_purple","bold":true}]
tellraw @a ["  ",{"text": "第一名 >  ","color":"red"},{"selector": "@a[tag=total_rank1]","color":"red"},{"text":" (","color":"gray"},{"score":{"name": "@p[tag=total_rank1]","objective": "round_score"},"color":"gray"},{"text":")","color":"gray"}]
execute if entity @a[tag=total_rank2] run tellraw @a ["  ",{"text": "第二名 >  ","color":"gold"},{"selector": "@a[tag=total_rank2]","color":"gold"},{"text":" (","color":"gray"},{"score":{"name": "@p[tag=total_rank2]","objective": "round_score"},"color":"gray"},{"text":")","color":"gray"}]
execute if entity @a[tag=total_rank3] run tellraw @a ["  ",{"text": "第三名 >  ","color":"yellow"},{"selector": "@a[tag=total_rank3]","color":"yellow"},{"text":" (","color":"gray"},{"score":{"name": "@p[tag=total_rank3]","objective": "round_score"},"color":"gray"},{"text":")","color":"gray"}]
tellraw @a ""

# 本局  K/D 计算
execute as @a run scoreboard players operation @s temp = @s round_kill
scoreboard players operation @a temp *= $1000 const
execute as @a run scoreboard players operation @s temp /= @s round_death

# 本局平均表现分
execute as @a[tag=playing] run scoreboard players operation $average_score temp += @s round_score
execute store result score $playing_count temp if entity @a[tag=playing]
scoreboard players operation $average_score temp /= $playing_count temp

# 个人结算内容
execute as @a[tag=playing,scores={round_kill=0}] run tellraw @s [" ",{"text": "本局击杀...你小子§6没有击杀§f！（是在偷懒吧？awa"}]
execute as @a[tag=playing,scores={round_kill=1..}] run tellraw @s [" ",{"text": "本局击杀了 §6"},{"score":{"objective": "round_kill","name": "@s"}}," 只玩家§f！"]
execute as @a[tag=playing,scores={round_death=0}] run tellraw @s [" ",{"text": "好家伙，你小子§60死§f！"}]
execute as @a[tag=playing,scores={round_death=1..}] run tellraw @s [" ",{"text": "本局死亡 §6"},{"score":{"objective": "round_death","name": "@s"}}," 次§f！"]
execute as @a[tag=playing] run tellraw @s [" ",{"text": "本局 K/D*1k："},{"score":{"objective": "temp","name": "@s"}},{"text": " | 表现分 "},{"score":{"objective": "round_score","name": "@s"}},{"text": "（本局平均分：","color": "gray"},{"score":{"name": "$average_score","objective": "temp"},"color": "gray"},{"text": "）","color": "gray"}]      
execute unless score $no_winner temp matches 1 run tellraw @a[tag=playing,tag=win] [" ",{"text": "本场比赛你们获得了§6胜利§f，再接再厉！"}]
execute unless score $no_winner temp matches 1 run tellraw @a[tag=playing,tag=!win] [" ",{"text": "本场比赛虽然失败，但是§6不要气馁，下次比赛结果会更好§f！"}]
execute if score $no_winner temp matches 1 run tellraw @a[tag=playing]  [" ",{"text": "本局比赛§6没有赢家§f！"}]
tellraw @a[tag=playing] ""

scoreboard players reset @a temp
scoreboard players reset $playing_count
scoreboard players reset $average_score

# 清除游戏区域和残留物品
fill -149 641 -149 149 45 149 air 
kill @e[type=item,tag=!bypass_kill]

function bw:global/clear_entity

# （地图接口）游戏结束
setblock 4 50 -32 minecraft:redstone_block

# 重置玩家 id
scoreboard players reset * player_id
scoreboard players set $ player_id 0

# 清理残留数据
execute as @a run attribute @s generic.attack_speed base set 4

tag @a remove lose_bed
tag @a remove playing

scoreboard players reset @a respawn_time
scoreboard players reset @a temp
scoreboard players reset * respawn_time
scoreboard players reset @a round_score
scoreboard players reset @a round_death
scoreboard players reset @a round_kill
scoreboard players reset @a round_combo

scoreboard players reset $size border 
scoreboard players reset $countdown gaming

scoreboard players reset $working mutation
scoreboard players reset $bed_break_check gamerule
scoreboard players reset $active player_count
scoreboard players reset $game_playing player_count
scoreboard players reset $timing gaming
scoreboard players reset $shrinking border

scoreboard players reset $red_kill team
scoreboard players reset $blue_kill team
scoreboard players reset $green_kill team
scoreboard players reset $yellow_kill team
scoreboard players reset $red_bed team
scoreboard players reset $blue_bed team
scoreboard players reset $green_bed team
scoreboard players reset $yellow_bed team

# 载入大厅状态
function bw:lobby/setup
execute as @a[tag=!afk,team=!debug] at @s run function lib:sounds/theend
forceload add 0 0