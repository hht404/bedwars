# 加入游戏时的介绍
execute if score $working mutation matches 1 run tellraw @s [{"text": " ✦ ","color": "aqua"},{"text": "内忧外患：背叛还是团结，你可以破坏自家的床..."},"\n"]
execute if score $working mutation matches 2 run tellraw @s [{"text": " ✦ ","color": "aqua"},{"text": "弃床战争：是大地的不公还是运气的不好，你将不再拥有床..."},"\n"]
execute if score $working mutation matches 3 run tellraw @s [{"text": " ✦ ","color": "aqua"},{"text": "无法自疗：自然回血的停用，伤口止不住的流血... 小心，伙计！"},"\n"]
execute if score $working mutation matches 4 run tellraw @s [{"text": " ✦ ","color": "aqua"},{"text": "追踪火球：你的火球将自动追踪到指定玩家！（使用追溯指针来确认玩家）"},"\n"]
execute if score $working mutation matches 5 run tellraw @s [{"text": " ✦ ","color": "aqua"},{"text": "两极反转：击杀冷却正在消失，准备回溯到低版本的 PVP 方式了吗？"},"\n"]
execute if score $working mutation matches 6 run tellraw @s [{"text": " ✦ ","color": "aqua"},{"text": "爆炸箭矢：哭声，喊声，爆炸声！落地的箭矢将会爆炸..."},"\n"]
execute if score $working mutation matches 7 run tellraw @s [{"text": " ✦ ","color": "aqua"},{"text": "鸡尾酒：来品尝狂乱的鸡尾酒吧！（每 10 秒获得随机药水效果）"},"\n"]