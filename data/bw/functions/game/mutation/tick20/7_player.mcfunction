# 给予玩家随机效果

# 随机抽取
scoreboard players set $random_min temp 1
scoreboard players set $random_max temp 10
function lib:random

# 给予效果与提示
execute if score $random temp matches 1 run effect give @s invisibility 10 1
execute if score $random temp matches 1 run title @s actionbar "随机事件已触发！获得效果：隐身"

execute if score $random temp matches 2 run effect give @s jump_boost 10 1
execute if score $random temp matches 2 run title @s actionbar "随机事件已触发！获得效果：跳跃提升"

execute if score $random temp matches 3 run effect give @s slowness 10 4
execute if score $random temp matches 3 run effect give @s resistance 10 3
execute if score $random temp matches 3 run title @s actionbar "随机事件已触发！获得效果：神龟"

execute if score $random temp matches 4 run effect give @s poison 10 1
execute if score $random temp matches 4 run title @s actionbar "随机事件已触发！获得效果：中毒"

execute if score $random temp matches 5 run effect give @s regeneration 10 1
execute if score $random temp matches 5 run title @s actionbar "随机事件已触发！获得效果：生命恢复"

execute if score $random temp matches 6 run effect give @s strength 10 1
execute if score $random temp matches 6 run title @s actionbar "随机事件已触发！获得效果：力量"

execute if score $random temp matches 7 run effect give @s weakness 10 1
execute if score $random temp matches 7 run title @s actionbar "随机事件已触发！获得效果：虚弱"

execute if score $random temp matches 8 run effect give @s slow_falling 10 1
execute if score $random temp matches 8 run title @s actionbar "随机事件已触发！获得效果：缓降"

execute if score $random temp matches 9 run effect give @s speed 10 1
execute if score $random temp matches 9 run title @s actionbar "随机事件已触发！获得效果：迅捷"

execute if score $random temp matches 10 run effect give @s fire_resistance 10 1
execute if score $random temp matches 10 run title @s actionbar "随机事件已触发！获得效果：抗火"

# 收尾
scoreboard players reset $random temp
scoreboard players reset $random_max temp
scoreboard players reset $random_min temp