# 每 2 Tick 执行一次

# 追踪火球
execute if score $working mutation matches 4 run function bw:game/mutation/tick2/4

# 爆炸箭矢
execute if score $working mutation matches 6 as @e[type=arrow,nbt={inGround:true}] at @s run function bw:game/mutation/tick2/6
