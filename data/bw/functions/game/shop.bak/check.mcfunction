# 检测玩家是否预想购买物品并且导航
# 如果身上有商店物品则符合要求

# 获得商店物品id
execute store result score @s temp run data get entity @s Inventory[{tag:{shop:{}}}].tag.shop.id

# 匹配物品
## 方块类（1=羊毛 2=末地石 3=梯子）
execute if score @s temp matches 1 run function bw:game/shop/item/block/wool
execute if score @s temp matches 2 run function bw:game/shop/item/block/end_stone
execute if score @s temp matches 3 run function bw:game/shop/item/block/ladder

# 收尾
scoreboard players reset @s temp