# 玩家试图购买羊毛

# 先决条件判定和提示
execute store result score @s temp run clear @s copper_ingot 0

## 如果符合
execute if score @s temp matches 4.. run give @s[team=blue] blue_wool 4
execute if score @s temp matches 4.. run give @s[team=green] green_wool 4
execute if score @s temp matches 4.. run give @s[team=yellow] yellow_wool 4
execute if score @s temp matches 4.. run give @s[team=red] red_wool 4
execute if score @s temp matches 4.. run clear @s copper_ingot 4
execute if score @s temp matches 4.. run function lib:sounds/hit

## 如果不符合
execute unless score @s temp matches 4.. run function lib:sounds/error

# 清除代表物
clear @s #wool{shop:{id:1b}}