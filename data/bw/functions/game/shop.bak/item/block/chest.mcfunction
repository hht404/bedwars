# 玩家试图购买箱子

# 先决条件判定和提示
execute store result score @s temp run clear @s copper_ingot 0

## 如果符合
execute if score @s temp matches 16.. run give @s chest 1
execute if score @s temp matches 16.. run clear @s copper_ingot 16
execute if score @s temp matches 16.. run function lib:sounds/hit

## 如果不符合
execute unless score @s temp matches 16.. run function lib:sounds/error

# 清除代表物
clear @s end_stone{shop:{id:2b}}