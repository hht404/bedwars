# 玩家试图购买木板

# 先决条件判定和提示
execute store result score @s temp run clear @s copper_ingot 0

## 如果符合
execute if score @s temp matches 8.. run give @s[team=blue] oak_planks 16
execute if score @s temp matches 8.. run clear @s copper_ingot 8
execute if score @s temp matches 8.. run function lib:sounds/hit

## 如果不符合
execute unless score @s temp matches 16.. run function lib:sounds/error

# 清除代表物
clear @s #wool{shop:{id:1b}}