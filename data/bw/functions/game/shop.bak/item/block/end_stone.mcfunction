# 玩家试图购买末地石

# 先决条件判定和提示
execute store result score @s temp run clear @s iron_ingot 0

## 如果符合
execute if score @s temp matches 4.. run give @s end_stone 4
execute if score @s temp matches 4.. run clear @s iron_ingot 4
execute if score @s temp matches 4.. run function lib:sounds/hit

## 如果不符合
execute unless score @s temp matches 4.. run function lib:sounds/error

# 清除代表物
clear @s end_stone{shop:{id:2b}}