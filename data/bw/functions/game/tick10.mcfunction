# 每 10 Tick 执行一次

# 大厅保护
title @a[x=-200,y=34,z=-200,dx=400,dy=10,dz=400,gamemode=survival,tag=playing] title [{"text": "大厅保护警告","bold": true,"color": "yellow"}]
title @a[x=-200,y=34,z=-200,dx=400,dy=10,dz=400,gamemode=survival,tag=playing] subtitle {"text": "⚠ 继续向上将予以死亡 ⚠","color": "yellow"}
execute as @a[x=-200,y=34,z=-200,dx=400,dy=10,dz=400,gamemode=survival,tag=playing] at @s run function lib:sounds/hit
kill @a[x=-200,y=40,z=-200,dx=400,dy=4,dz=400,gamemode=survival,tag=playing]

# 虚空伤害（y=-128）
execute if score $void_damage gamerule matches 1 as @a[x=-200,y=-200,z=-200,dx=400,dy=72,dz=400,gamemode=survival] run damage @s 8 out_of_world
execute if score $void_damage gamerule matches 2 as @a[x=-200,y=-200,z=-200,dx=400,dy=100,dz=400,gamemode=survival] run damage @s 114514 out_of_world

# 如果有玩家合成：立即调用合成检测
execute as @a[scores={craft_wool=1..}] run function bw:game/craft/check
scoreboard players reset @a craft_wool

# 特殊物品的nbt设定
item modify entity @a[tag=playing,nbt={SelectedItem:{id: "minecraft:wooden_axe"}}] weapon.mainhand bw:special_items/wooden_axe
item modify entity @a[tag=playing,nbt={SelectedItem:{id: "minecraft:tnt"}}] weapon.mainhand bw:special_items/tnt
item modify entity @a[tag=playing,nbt={Inventory:[{Slot:-106b,id: "minecraft:tnt"}]}] weapon.offhand bw:special_items/tnt

# 附魔系统
execute as @e[type=item,nbt={Item:{id:"minecraft:enchanted_book"}}] at @s if entity @e[distance=0.1..0.8,type=item,nbt=!{Item:{id:"minecraft:enchanted_book"}}] run data modify entity @e[sort=nearest,limit=1,type=item,nbt=!{Item:{id:"minecraft:enchanted_book"}}] Item.tag.Enchantments append from entity @s Item.tag.StoredEnchantments[{}]
execute as @e[type=item,nbt={Item:{id:"minecraft:enchanted_book"}}] at @s if entity @e[distance=0.1..0.8,type=item,nbt=!{Item:{id:"minecraft:enchanted_book"}}] run kill @s