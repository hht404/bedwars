# 每 2 Tick 执行一次

# 调用资源点模块
# function bw:game/resource/tick2

# 大厅异物保护
kill @e[x=-200,y=40,z=-200,dx=400,dy=4,dz=400,type=!player]

# 攻击他人将显示血量
execute as @a[scores={health=5..}] at @s on attacker run title @s actionbar [{"selector":"@p"},{"text": " HP >> "},{"score":{"objective":"health","name":"@p","bold": false}}]
execute as @a[scores={health=..4}] at @s on attacker run title @s actionbar [{"selector":"@p","underlined": true},{"text": " HP >> "},{"score":{"objective":"health","name":"@p"}}]

# 破坏床的检测
function bw:game/bed/break

# 边境管理
execute as @e[type=marker,tag=border] at @s run function bw:game/border/tick2

# 调用随机事件（如果有的话）
execute if score $working mutation matches 1.. run function bw:game/mutation/tick2

# 调用特殊物品
function bw:game/special_items/tick2

# 调用商店
execute if score $exchange gamerule matches 2 run function bw:game/shop/tick2