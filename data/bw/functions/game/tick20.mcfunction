# 每 20 Tick 执行一次

# 游戏倒计时
scoreboard players remove $countdown gaming 1

# 游戏结束检测（倒计时）
execute if score $countdown gaming matches 0 run function bw:game/game_end

# 同步bossbar
execute store result bossbar countdown:blue value run scoreboard players get $countdown gaming
execute store result bossbar countdown:green value run scoreboard players get $countdown gaming
execute store result bossbar countdown:red value run scoreboard players get $countdown gaming
execute store result bossbar countdown:yellow value run scoreboard players get $countdown gaming
execute store result bossbar countdown:spec value run scoreboard players get $countdown gaming

# 时间事件
## 正计时
scoreboard players add $timing gaming 1
## 提醒事件发生
execute if score $timing gaming = $timing_bed_break gaming run tellraw @a[tag=playing] [{"text": "[⌚] ","color": "aqua","bold": true},{"text": "距离床自毁还有 ","bold": false},{"score":{"objective": "timing","name": "$timing_notice"},"bold": false},{"text": " 秒","bold": false}]
execute if score $timing gaming = $timing_border_shrink gaming if score $border gamerule matches 1 run tellraw @a[tag=playing] [{"text": "[⌚] ","color": "aqua","bold": true},{"text": "距离边界收缩还有 ","bold": false},{"score":{"objective": "timing","name": "$timing_notice"},"bold": false},{"text": " 秒","bold": false}]
## 时间触发
execute if score $timing gaming = $timing_bed_break gaming run function bw:game/timing_event/bed_break
execute if score $timing gaming = $timing_border_shrink gaming if score $border gamerule matches 1 run function bw:game/timing_event/border_shrink

# 复活
execute as @a[scores={respawn_time=0..}] run function bw:game/respawn/respawning

# 调用资源点模块
function bw:game/resource/tick20

# 边界管理
execute as @e[type=marker,tag=border] at @s run function bw:game/border/tick20

# 防止旁观者想不开
execute as @a[gamemode=spectator,tag=playing,x=-200,y=-200,z=-200,dx=400,dy=80,dz=400] run function bw:game/map/enter

# 床受到意外破坏后重恢复
execute at @e[type=marker,tag=blue_bed] unless block ~ ~ ~ blue_bed if score $blue_bed team matches 1 run function bw:game/bed/team/blue_restore
execute at @e[type=marker,tag=green_bed] unless block ~ ~ ~ green_bed if score $green_bed team matches 1 run function bw:game/bed/team/green_restore
execute at @e[type=marker,tag=red_bed] unless block ~ ~ ~ red_bed if score $red_bed team matches 1 run function bw:game/bed/team/red_restore
execute at @e[type=marker,tag=yellow_bed] unless block ~ ~ ~ yellow_bed if score $yellow_bed team matches 1 run function bw:game/bed/team/yellow_restore

# 玩家在家中获得正面buff
# execute as @e[type=marker,tag=bed] at @s as @a[tag=playing,distance=1..12] run effect give @s regeneration 2 0 true

# 如果一个队伍没有人则触发破坏逻辑
function bw:game/team_update
summon marker 0 0 0 {CustomName:'{"text":"系统"}',Tags:["bed","temp"]}
execute if score $red_bed team matches 1 unless score $red_number team matches 1.. as @e[type=marker,tag=bed,tag=temp] run function bw:game/bed/team/red
execute if score $green_bed team matches 1 unless score $green_number team matches 1.. as @e[type=marker,tag=bed,tag=temp] run function bw:game/bed/team/green
execute if score $yellow_bed team matches 1 unless score $yellow_number team matches 1.. as @e[type=marker,tag=bed,tag=temp] run function bw:game/bed/team/yellow
execute if score $blue_bed team matches 1 unless score $blue_number team matches 1.. as @e[type=marker,tag=bed,tag=temp] run function bw:game/bed/team/blue
kill @e[type=marker,tag=temp,tag=bed]

# 调用随机事件（如果有的话）
execute if score $working mutation matches 1.. run function bw:game/mutation/tick20

# 调用特殊物品
function bw:game/special_items/tick20

# 检测购买项目
function bw:game/craft/check

# （地图接口）每秒尝试放置红石块
setblock 2 50 -32 minecraft:redstone_block

# 自我矫正
## 资源点缺少 res_cd：更新 cd 为 1
execute as @e[type=armor_stand,tag=res] unless score @s res_cd = @s res_cd run scoreboard players set @s res_cd 1