# 游戏载入和开始

# 游戏规则
gamerule fallDamage true
gamerule naturalRegeneration true
execute if score $map_using map matches 3 run gamerule doFireTick false
execute unless score $map_using map matches 3 run gamerule doFireTick true
execute if score $daylight_cycle gamerule matches 1 run gamerule doDaylightCycle true
execute unless score $daylight_cycle gamerule matches 1 run gamerule doDaylightCycle false

# 时间设定
time set day

# 最大人数限制（预选玩家拥有player标签）
scoreboard players set $playing_count temp 0
execute store result score $playing_count temp if entity @a[tag=player]
execute if score $playing_count temp > $player_max team run tellraw @a[tag=player] ["",{"text": "[⚠] ","color": "yellow","bold": true}, {"text": "人数已经超过最大人数限制！将随机挑选几个幸运玩家踢出游戏...","color": "yellow"}]

tag @a remove select_player
execute unless score $playing_count temp > $player_max team store result score $select_player temp if entity @a[tag=player]
execute if score $playing_count temp > $player_max team store result score $select_player temp run scoreboard players get $player_max team
function bw:game/system_init/select_player
scoreboard players reset $select_player temp

tellraw @a[tag=!select_player,tag=player] ["",{"text": ">> ","color": "red","bold": true}, {"text": "很遗憾...你被抛弃了！","color": "red"}]
execute as @a[tag=!select_player,tag=player] run function bw:game/player_event/spec_join
tag @a[tag=!select_player,tag=player] remove player

tag @a remove select_player

# 将大厅玩家转换为旁观者
execute as @a[team=lobby,tag=!player] run function bw:game/player_event/spec_join

# 清理残留
## 系统数据
scoreboard players reset @a rejoin_trigger
tag @a remove lose_bed
## 末影箱和物品
clear @a[tag=player]

item replace entity @a[tag=player] enderchest.0 with air
item replace entity @a[tag=player] enderchest.1 with air
item replace entity @a[tag=player] enderchest.2 with air
item replace entity @a[tag=player] enderchest.3 with air
item replace entity @a[tag=player] enderchest.4 with air
item replace entity @a[tag=player] enderchest.5 with air
item replace entity @a[tag=player] enderchest.6 with air
item replace entity @a[tag=player] enderchest.7 with air
item replace entity @a[tag=player] enderchest.8 with air
item replace entity @a[tag=player] enderchest.9 with air
item replace entity @a[tag=player] enderchest.10 with air
item replace entity @a[tag=player] enderchest.11 with air
item replace entity @a[tag=player] enderchest.12 with air
item replace entity @a[tag=player] enderchest.13 with air
item replace entity @a[tag=player] enderchest.14 with air
item replace entity @a[tag=player] enderchest.15 with air
item replace entity @a[tag=player] enderchest.16 with air
item replace entity @a[tag=player] enderchest.17 with air
item replace entity @a[tag=player] enderchest.18 with air
item replace entity @a[tag=player] enderchest.19 with air
item replace entity @a[tag=player] enderchest.20 with air
item replace entity @a[tag=player] enderchest.21 with air
item replace entity @a[tag=player] enderchest.22 with air
item replace entity @a[tag=player] enderchest.23 with air
item replace entity @a[tag=player] enderchest.24 with air
item replace entity @a[tag=player] enderchest.25 with air
item replace entity @a[tag=player] enderchest.26 with air

# 随机分队
execute store result score $team temp if entity @a[tag=player]
team leave @a[tag=player]
function bw:game/system_init/team_join
scoreboard players reset $team temp

tellraw @a[team=red] [{"text": "[★] ","color": "aqua","bold": true},{"text": "您已分配至 ","bold": false},{"text": "红队","color": "white"}]
tellraw @a[team=blue] [{"text": "[★] ","color": "aqua","bold": true},{"text": "您已分配至 ","bold": false},{"text": "蓝队","color": "white"}]
tellraw @a[team=yellow] [{"text": "[★] ","color": "aqua","bold": true},{"text": "您已分配至 ","bold": false},{"text": "黄队","color": "white"}]
tellraw @a[team=green] [{"text": "[★] ","color": "aqua","bold": true},{"text": "您已分配至 ","bold": false},{"text": "绿队","color": "white"}]
tellraw @a[team=spec] [{"text": "[✖] ","color": "yellow","bold": true},{"text": "所有队伍都已满人，进入旁观状态！ ","bold": false}]

tag @a[tag=player] remove player

# 玩家数据控制
tag @a[team=red] add playing
tag @a[team=yellow] add playing
tag @a[team=blue] add playing
tag @a[team=green] add playing
tag @a[team=spec] add playing

# 随机事件抽取（大部分时间没有随机事件）（如果有了则跳过）
execute unless score $working mutation matches 1.. run function bw:game/system_init/mutation_get

# 边境管理
summon marker 0 0 0 {Tags: ["border"]}

# 清除微观模型
fill 11 44 49 -11 67 71 air
kill @e[type=item,tag=!bypass_kill]

# 初始化床存在数据（有玩家的前提下）
function bw:game/team_update
execute if score $blue_number team matches 1.. run scoreboard players set $blue_bed team 1
execute unless score $blue_number team matches 1.. run scoreboard players set $blue_bed team 2
execute if score $red_number team matches 1.. run scoreboard players set $red_bed team 1
execute unless score $red_number team matches 1.. run scoreboard players set $red_bed team 2
execute if score $green_number team matches 1.. run scoreboard players set $green_bed team 1
execute unless score $green_number team matches 1.. run scoreboard players set $green_bed team 2
execute if score $yellow_number team matches 1.. run scoreboard players set $yellow_bed team 1
execute unless score $yellow_number team matches 1.. run scoreboard players set $yellow_bed team 2

# 拆家检测初始化（玩家无法破坏自家的床）
scoreboard players set $bed_break_check gamerule 1

# 地图载入
function bw:game/map/init/trigger

# 同步游戏倒计时
scoreboard players operation $countdown_max gaming = $countdown_gaming gamerule
scoreboard players operation $countdown gaming = $countdown_max gaming

# 初始化资源点产出时间
execute as @e[type=armor_stand,tag=res_iron] store result score @s res_cd run scoreboard players get $res_iron gamerule
execute as @e[type=armor_stand,tag=res_copper] store result score @s res_cd run scoreboard players get $res_copper gamerule
execute as @e[type=armor_stand,tag=res_diamond] store result score @s res_cd run scoreboard players get $res_diamond gamerule

# 选中商店
execute as @e[type=marker,tag=shop] if score @s shop = $shop_using shop run tag @s add shop_selected

# Bossbar 更新
bossbar set countdown:blue players
bossbar set countdown:green players
bossbar set countdown:yellow players
bossbar set countdown:red players
bossbar set countdown:spec players

execute store result bossbar countdown:blue max run scoreboard players get $countdown_max gaming
execute store result bossbar countdown:green max run scoreboard players get $countdown_max gaming
execute store result bossbar countdown:red max run scoreboard players get $countdown_max gaming
execute store result bossbar countdown:yellow max run scoreboard players get $countdown_max gaming
execute store result bossbar countdown:spec max run scoreboard players get $countdown_max gaming

# 时间事件初始化（提前提醒数值）
scoreboard players operation $timing_border_shrink gaming = $border_shrink timing
scoreboard players operation $timing_border_shrink gaming -= $notice timing
scoreboard players operation $timing_bed_break gaming = $bed_break timing
scoreboard players operation $timing_bed_break gaming -= $notice timing

# HUB 控制
scoreboard objectives setdisplay belowName health
scoreboard objectives setdisplay list health
scoreboard objectives setdisplay sidebar
scoreboard objectives setdisplay sidebar.team.blue round_info
scoreboard objectives setdisplay sidebar.team.green round_info
scoreboard objectives setdisplay sidebar.team.red round_info
scoreboard objectives setdisplay sidebar.team.yellow round_info
scoreboard objectives setdisplay sidebar.team.gray round_info

# 检测环境 
scoreboard players set $env_check temp 1
execute unless score $testing gaming matches 1 store result score $env_check temp run function bw:game/system_init/env_check
execute if score $env_check temp matches 2.. run function bw:game/system_init/env_check_fail
execute if score $env_check temp matches 2.. run return 1
execute if score $env_check temp matches 1.. run scoreboard players reset $env_check temp
    
# （地图接口）游戏开始
setblock 6 50 -32 minecraft:redstone_block

# 测试局设定
execute if score $testing gaming matches 1 run scoreboard players set $countdown gaming -1

# 初始化随机事件（5秒后）
schedule function bw:game/mutation/game_start/trigger 5s replace

# 初始化完成，系统载入：游戏中！
scoreboard players set $working gaming 1

# 发送提示
tellraw @a ["",{"text": "[⏻] ","color": "aqua","bold": true}, {"text": "游戏开始！","color": "aqua"}]

# 玩家载入
execute as @a[tag=playing] run function bw:game/player_event/enter
