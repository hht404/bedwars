# 判定玩家是否在边境外（正数=在内 负数=在外）
execute store result score $border temp run scoreboard players get $size border 
scoreboard players operation $border temp *= $border temp

execute store result score $border_player_x temp run data get entity @s Pos[0]
scoreboard players operation $border_player_x temp *= $border_player_x temp
execute store result score $border_player_z temp run data get entity @s Pos[2]
scoreboard players operation $border_player_z temp *= $border_player_z temp
scoreboard players operation $border_player temp = $border_player_x temp
scoreboard players operation $border_player temp += $border_player_z temp

scoreboard players operation $border temp -= $border_player temp
scoreboard players operation @s temp = $border temp

# 数据标记
tag @s remove out_of_border
tag @s remove out_of_world
tag @s[scores={temp=..0}] add out_of_border
tag @s[scores={temp=..-9600}] add out_of_world

# 重置数据
scoreboard players reset @s temp
scoreboard players reset $border temp
scoreboard players reset $border_player temp
scoreboard players reset $border_player_x temp
scoreboard players reset $border_player_z temp