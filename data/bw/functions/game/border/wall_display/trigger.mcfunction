# 边界墙体

# 同步玩家y轴
execute store result entity @e[type=marker,limit=1,tag=border] Pos[1] double 1 run data get entity @s Pos[1]

# 旋转显示
execute at @e[type=marker,tag=border] align y rotated ~ 0 run function bw:game/border/wall_display/trigger_2
execute at @e[type=marker,tag=border] align y rotated ~90 0 run function bw:game/border/wall_display/trigger_2
execute at @e[type=marker,tag=border] align y rotated ~180 0 run function bw:game/border/wall_display/trigger_2
execute at @e[type=marker,tag=border] align y rotated ~270 0 run function bw:game/border/wall_display/trigger_2