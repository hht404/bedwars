scoreboard players remove $map_iron_spawner temp 1
summon marker ~ ~ ~ {Tags:["res","res_iron","sys_editing"]}
data modify entity @e[type=marker,tag=sys_editing,limit=1] Pos set from storage bw:map spawner.iron[0]
data remove storage bw:map spawner.iron[0]
tag @e[type=marker,tag=sys_editing] remove sys_editing

execute if score $map_iron_spawner temp matches 1.. run function bw:game/map/init/spawner/iron