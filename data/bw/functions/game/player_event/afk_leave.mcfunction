# 移除挂机玩家
# as @a[score={afk_time=180..}]

# 数据操控
function bw:game/player_event/spec_join

# 发送提示
tellraw @a[distance=0.1..] [{"text": "[⏳] ","bold": true,"color": "yellow"},{"selector":"@s","bold": false},{"text": " 因为长时间挂机而离开了！","color": "yellow","bold": false}]
tellraw @s [{"text": "[⏳] ","bold": true,"color": "yellow"},{"text": "检测到你长时间处于挂机状态，已经自动离开游戏！","color": "yellow","bold": false}]