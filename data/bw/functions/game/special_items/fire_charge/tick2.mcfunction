# 召唤火球
execute as @e[type=minecraft:snowball] at @s on origin anchored eyes summon fireball run function bw:game/special_items/fire_charge/set_data

# 火球寿命
execute as @e[type=fireball] run scoreboard players add @s fireball_life 1
execute as @e[type=fireball] if score @s fireball_life >= $life_max fireball_life run kill @s