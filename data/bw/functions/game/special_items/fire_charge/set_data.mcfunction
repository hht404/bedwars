data modify entity @s Owner set from entity @e[type=snowball,limit=1,sort=nearest] Owner
execute on origin run damage @e[type=minecraft:fireball,sort=nearest,limit=1] 1 minecraft:arrow by @s
data modify entity @s NoGravity set value true
data modify entity @s ExplosionPower set value 1
data modify entity @s Item merge value {id: "minecraft:snowball", Count:1b}
data modify entity @s Item.tag set from entity @e[type=minecraft:snowball,sort=nearest,limit=1] Item.tag


kill @e[type=snowball,sort=nearest,limit=1] 