# 追溯指针

# 玩家选择
execute as @a[nbt={Inventory:[{Slot:-106b,id:"minecraft:recovery_compass"}]}] at @s run function bw:game/special_items/recovery_compass/select
execute as @a[nbt={Inventory:[{Slot:-106b,id:"minecraft:recovery_compass"}]}]

# 玩家使用（手持）
execute as @a[nbt={SelectedItem:{id:"minecraft:recovery_compass"}}] at @s if score @s player_id = @s player_id run function bw:game/special_items/recovery_compass/use

execute as @a if score @s player_id = @s compass_select run scoreboard players add @s compass_select 1
execute as @a if score @s compass_select >= $ player_id run scoreboard players set @s compass_select 0


# 盔甲架自毁
execute as @e[type=armor_stand,tag=recovery_compass] at @s unless entity @a[distance=..1] run kill @s 