# 玩家在使用追溯指针 尝试切换玩家时触发

# 道具位置更新
item replace entity @s weapon.offhand with air
item replace entity @s[nbt=!{SelectedItem:{}}] weapon.mainhand with minecraft:recovery_compass 1
give @s[nbt=!{SelectedItem:{id:"minecraft:recovery_compass"}}] minecraft:recovery_compass

# 数据操控
scoreboard players add @s compass_select 1
execute if score @s player_id = @s compass_select run scoreboard players add @s compass_select 1
execute if score @s compass_select >= $ player_id run scoreboard players set @s compass_select 0

# 如果没有选择到在线的玩家则跳过这一玩家（循环）
tag @a remove compass_allow
execute as @a[gamemode=!spectator,tag=playing] if score @s player_id = @p compass_select run tag @p add compass_allow
execute as @s[tag=!compass_allow] run function bw:game/special_items/recovery_compass/select_loop