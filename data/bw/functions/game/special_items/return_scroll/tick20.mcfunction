# 回城卷轴（别问英文名称这么奇怪（捂脸

# 倒计时&显示剩余时间
execute as @a[nbt={Inventory:[{Slot:-106b,id:"minecraft:gunpowder"}]}] run function bw:game/special_items/return_scroll/waiting

# 重置倒计时
scoreboard players set @a[predicate=lib:sprinting] return_scroll 5
scoreboard players set @a[nbt=!{Inventory:[{Slot:-106b,id:"minecraft:gunpowder"}]}] return_scroll 5

# 触发返回
execute as @a[nbt={Inventory:[{Slot:-106b,id:"minecraft:gunpowder"}]},scores={return_scroll=0}] at @s run function bw:game/special_items/return_scroll/trigger_back