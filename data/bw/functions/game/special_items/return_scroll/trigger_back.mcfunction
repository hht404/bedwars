# 回城卷轴 - 触发返回
effect give @s minecraft:resistance 1 5
function bw:game/map/enter
title @s title "已返回"
title @s subtitle ""
function lib:sounds/hit2

# item replace entity @s weapon.mainhand from entity @s weapon.offhand
# item replace entity @s weapon.offhand with air
clear @s minecraft:gunpowder 1

# 重置倒计时
scoreboard players set @s return_scroll 5