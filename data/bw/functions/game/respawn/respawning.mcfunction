# 重生中

# 倒计时
scoreboard players remove @s respawn_time 1

# 发送复活倒计时
execute unless score @s respawn_time = $ respawn_time run title @s title [{"score":{"objective": "respawn_time","name": "@s"}}]
execute unless score @s respawn_time = $ respawn_time run title @s subtitle ["复活中..."]

# 复活完成
execute if score @s respawn_time matches 0 run function bw:game/respawn/respawn_complete