# 更新本局信息（右侧计分板）

# 更新队伍信息
function bw:game/team_update

# 遍历！
execute if score $red_bed team matches 1 run team modify red_info suffix " [✔]"
execute if score $red_bed team matches 2 if score $red_number team matches 1 run team modify red_info suffix " [1只玩家]"
execute if score $red_bed team matches 2 if score $red_number team matches 2 run team modify red_info suffix " [2只玩家]"
execute if score $red_bed team matches 2 if score $red_number team matches 3 run team modify red_info suffix " [3只玩家]"
execute if score $red_bed team matches 2 if score $red_number team matches 4 run team modify red_info suffix " [4只玩家]"
execute if score $red_bed team matches 2 if score $red_number team matches 5 run team modify red_info suffix " [5只玩家]"
execute if score $red_bed team matches 2 if score $red_number team matches 6 run team modify red_info suffix " [6只玩家]"
execute if score $red_bed team matches 2 unless score $red_number team matches 1.. run team modify red_info suffix " [✘]"

execute if score $blue_bed team matches 1 run team modify blue_info suffix " [✔]"
execute if score $blue_bed team matches 2 if score $blue_number team matches 1 run team modify blue_info suffix " [1只玩家]"
execute if score $blue_bed team matches 2 if score $blue_number team matches 2 run team modify blue_info suffix " [2只玩家]"
execute if score $blue_bed team matches 2 if score $blue_number team matches 3 run team modify blue_info suffix " [3只玩家]"
execute if score $blue_bed team matches 2 if score $blue_number team matches 4 run team modify blue_info suffix " [4只玩家]"
execute if score $blue_bed team matches 2 if score $blue_number team matches 5 run team modify blue_info suffix " [5只玩家]"
execute if score $blue_bed team matches 2 if score $blue_number team matches 6 run team modify blue_info suffix " [6只玩家]"
execute if score $blue_bed team matches 2 unless score $blue_number team matches 1.. run team modify blue_info suffix " [✘]"

execute if score $green_bed team matches 1 run team modify green_info suffix " [✔]"
execute if score $green_bed team matches 2 if score $green_number team matches 1 run team modify green_info suffix " [1只玩家]"
execute if score $green_bed team matches 2 if score $green_number team matches 2 run team modify green_info suffix " [2只玩家]"
execute if score $green_bed team matches 2 if score $green_number team matches 3 run team modify green_info suffix " [3只玩家]"
execute if score $green_bed team matches 2 if score $green_number team matches 4 run team modify green_info suffix " [4只玩家]"
execute if score $green_bed team matches 2 if score $green_number team matches 5 run team modify green_info suffix " [5只玩家]"
execute if score $green_bed team matches 2 if score $green_number team matches 6 run team modify green_info suffix " [6只玩家]"
execute if score $green_bed team matches 2 unless score $green_number team matches 1.. run team modify green_info suffix " [✘]"

execute if score $yellow_bed team matches 1 run team modify yellow_info suffix " [✔]"
execute if score $yellow_bed team matches 2 if score $yellow_number team matches 1 run team modify yellow_info suffix " [1只玩家]"
execute if score $yellow_bed team matches 2 if score $yellow_number team matches 2 run team modify yellow_info suffix " [2只玩家]"
execute if score $yellow_bed team matches 2 if score $yellow_number team matches 3 run team modify yellow_info suffix " [3只玩家]"
execute if score $yellow_bed team matches 2 if score $yellow_number team matches 4 run team modify yellow_info suffix " [4只玩家]"
execute if score $yellow_bed team matches 2 if score $yellow_number team matches 5 run team modify yellow_info suffix " [5只玩家]"
execute if score $yellow_bed team matches 2 if score $yellow_number team matches 6 run team modify yellow_info suffix " [6只玩家]"
execute if score $yellow_bed team matches 2 unless score $yellow_number team matches 1.. run team modify yellow_info suffix " [✘]"

# 优先级设定
# 有床的位于首列
execute if score $red_bed team matches 1 run scoreboard players operation §c红队 round_info = $max_number team
execute if score $red_bed team matches 1 run scoreboard players operation §c红队 round_info += $1 const
execute if score $yellow_bed team matches 1 run scoreboard players operation §e黄队 round_info = $max_number team
execute if score $yellow_bed team matches 1 run scoreboard players operation §e黄队 round_info += $1 const
execute if score $blue_bed team matches 1 run scoreboard players operation §9蓝队 round_info = $max_number team
execute if score $blue_bed team matches 1 run scoreboard players operation §9蓝队 round_info += $1 const
execute if score $green_bed team matches 1 run scoreboard players operation §a绿队 round_info = $max_number team
execute if score $green_bed team matches 1 run scoreboard players operation §a绿队 round_info += $1 const

# 无床但有玩家的位列中间
execute if score $red_bed team matches 2 if score $red_number team matches 1.. run scoreboard players operation §c红队 round_info = $red_number team
execute if score $yellow_bed team matches 2 if score $yellow_number team matches 1.. run scoreboard players operation §e黄队 round_info = $yellow_number team
execute if score $blue_bed team matches 2 if score $blue_number team matches 1.. run scoreboard players operation §9蓝队 round_info = $blue_number team
execute if score $green_bed team matches 2 if score $green_number team matches 1.. run scoreboard players operation §a绿队 round_info = $green_number team

# 无床无玩家的位于最后
execute if score $red_bed team matches 2 unless score $red_number team matches 1.. run scoreboard players operation §c红队 round_info = $min_number team
execute if score $red_bed team matches 2 unless score $red_number team matches 1.. run scoreboard players operation §c红队 round_info -= $1 const
execute if score $yellow_bed team matches 2 unless score $yellow_number team matches 1.. run scoreboard players operation §e黄队 round_info = $min_number team
execute if score $yellow_bed team matches 2 unless score $yellow_number team matches 1.. run scoreboard players operation §e黄队 round_info -= $1 const
execute if score $blue_bed team matches 2 unless score $blue_number team matches 1.. run scoreboard players operation §9蓝队 round_info = $min_number team
execute if score $blue_bed team matches 2 unless score $blue_number team matches 1.. run scoreboard players operation §9蓝队 round_info -= $1 const
execute if score $green_bed team matches 2 unless score $green_number team matches 1.. run scoreboard players operation §a绿队 round_info = $min_number team
execute if score $green_bed team matches 2 unless score $green_number team matches 1.. run scoreboard players operation §a绿队 round_info -= $1 const
