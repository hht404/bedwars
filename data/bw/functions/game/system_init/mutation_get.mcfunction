scoreboard players operation $last_mutation mutation = $working mutation

scoreboard players set $random_min temp 0
scoreboard players set $random_max temp 13
function lib:random

execute if score $random temp matches 0 run function bw:game/system_init/mutation_get_loop
scoreboard players operation $working mutation = $random temp

scoreboard players reset $random_get temp
scoreboard players reset $random temp
scoreboard players reset $random_max temp
scoreboard players reset $random_min temp