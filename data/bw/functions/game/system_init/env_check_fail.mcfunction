# 环境变量检测失败

# 音效
execute as @a at @s run function lib:sounds/error

# 具体原因
execute if score $env_check temp matches 10 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，没有检测到任何资源点（ERROR CODE 10）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 20 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，边界实体丢失（ERROR CODE 20）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 30 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，没有检测到任何队伍床实体（ERROR CODE 30）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 31 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，床实体过多（ERROR CODE 30）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 40 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，没有任何预选或者游玩中玩家（ERROR CODE 40）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 51 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，黄队缺少出生点实体（ERROR CODE 51）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 52 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，红队缺少出生点实体（ERROR CODE 52）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 53 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，绿队缺少出生点实体（ERROR CODE 53）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 54 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，蓝队缺少出生点实体（ERROR CODE 54）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 55 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，出生点实体过多（ERROR CODE 55）。游戏启动已中断！稍后载入大厅模块...","bold": false}]
execute if score $env_check temp matches 60 run tellraw @a [{"text": "[✖] ","bold": true,"color": "red"},{"text": "环境检测失败，指令区域加载失败（ERROR CODE 60）。游戏启动已中断！稍后载入大厅模块...","bold": false}]

# 清除地图载入的实体
function bw:global/clear_entity

# 延迟载入大厅
schedule function bw:lobby/setup 5t replace

# 收尾
schedule clear bw:game/mutation/game_start/trigger
scoreboard players reset $bed_entity temp
scoreboard players reset $spawn_entity temp