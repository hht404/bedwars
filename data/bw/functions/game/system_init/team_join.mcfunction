team join blue @r[team=,tag=player]
team join green @r[team=,tag=player]
team join red @r[team=,tag=player]
team join yellow @r[team=,tag=player]

execute store result score $team temp if entity @a[tag=player,team=]
execute if score $team temp matches 1.. run function bw:game/system_init/team_join
