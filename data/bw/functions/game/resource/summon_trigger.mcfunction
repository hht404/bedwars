# 执行前判断
function bw:game/resource/check
execute if score $res_allow temp matches 1 run function bw:game/resource/summon
execute unless score $res_allow temp matches 1 run title @a[distance=0..10] actionbar {"text": "这个资源点堆积了过多的物品 因此无法生成物品！","color": "yellow","bold": true}
scoreboard players reset $res_allow temp

# 同步倒计时
execute as @s[tag=res_diamond] run scoreboard players operation @s res_cd = $res_diamond gamerule
execute as @s[tag=res_copper] run scoreboard players operation @s res_cd = $res_copper gamerule
execute as @s[tag=res_iron] run scoreboard players operation @s res_cd = $res_iron gamerule