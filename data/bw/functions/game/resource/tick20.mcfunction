# 每 20 Tick 执行一次 

# 倒计时
scoreboard players remove @e[type=armor_stand,tag=res,scores={res_cd=1..}] res_cd 1

# 召唤实体
execute as @e[type=armor_stand,scores={res_cd=0}] at @s run function bw:game/resource/summon_trigger

# 最后更新文本（不会出现0秒剩余）
summon text_display 0 319 0 {Tags:["res","res_display","temp"],Invulnerable:true}
execute as @e[type=armor_stand,tag=res] at @s run function bw:game/resource/text_update
kill @e[type=text_display,tag=res_display,tag=temp]