# 完成导入

tag @e[limit=1,tag=map_importer,type=marker,tag=map] remove map_importer

execute store result score $map_max map if entity @e[type=marker,tag=map,tag=!special_map]
execute as @e[type=marker,tag=map] run scoreboard players operation $map_min map < @s map
execute if entity @e[type=marker,tag=map,tag=special_map] if score $map_min map matches 0 run scoreboard players remove $map_min map 1

kill @e[type=marker,tag=map_center,tag=map_importer]

function debug:map_import/reset
tellraw @a [{"text": "[✔] ","color": "yellow","bold": true},{"text": "已结束导入程序！","bold": false}]