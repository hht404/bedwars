# 判定是否有实体

# 预先检测实体
execute if entity @e[type=marker,tag=map_importer] run tellraw @s [{"text": "[✖] ","color": "red","bold": true},{"text": "错误：已经存在了一个数据实体编辑中！","bold": false}]
execute if entity @e[type=marker,tag=map_importer] run return 1

# 尝试选择实体
execute as @e[type=marker,tag=map] if score @s map = @p mi_input run tag @s add map_importer

# 无实体
execute unless entity @e[type=marker,tag=map,tag=map_importer] run tellraw @s [{"text": "[✖] ","color": "red","bold": true},{"text": "错误！没能选择到一个实体...","bold": false}]
execute unless entity @e[type=marker,tag=map,tag=map_importer] run function lib:sounds/error
execute unless entity @e[type=marker,tag=map,tag=map_importer] run return 1

# 无id
execute as @e[type=marker,tag=map,tag=map_importer] unless data entity @s data.map.id run tellraw @p [{"text": "[✖] ","color": "red","bold": true},{"text": "错误！实体没有对应id...","bold": false}]
execute as @e[type=marker,tag=map,tag=map_importer] unless data entity @s data.map.id as @p at @s run function lib:sounds/error
execute as @e[type=marker,tag=map,tag=map_importer] unless data entity @s data.map.id run return 1

# 选择成功
execute as @e[type=marker,tag=map,tag=map_importer] if data entity @s data.map.id run tellraw @p [{"text": "[✔] ","bold": true,"color": "aqua"},{"text": "实体已试图选择！地图编号为 ","bold": false},{"nbt":"data.map.id","entity": "@e[type=marker,tag=map_importer,tag=map]","bold": true,"color": "white"},{"text": " 名称为：","bold": false},{"nbt":"data.map.name","entity": "@e[type=marker,tag=map_importer,tag=map]","color": "white","bold": true}]

# 重新召唤中心点
kill @e[type=marker,tag=map_center]
execute if data entity @e[type=marker,tag=map,tag=map_importer,limit=1] data.map.id summon marker run function debug:map_import/entity_event/summon_center

# 收尾
scoreboard players reset @s mi_input