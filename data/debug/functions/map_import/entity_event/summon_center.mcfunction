# 根据正在编辑的数据实体更新中心点
data modify entity @s Pos set from entity @e[tag=map,tag=map_importer,limit=1] data.map.center
data modify entity @s Tags set value ["map_center"]