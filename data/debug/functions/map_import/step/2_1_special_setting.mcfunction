# 设定地图id

tellraw @s ""
tellraw @s {"text": ">> 步骤2_1：特殊地图设定","bold": true,"color": "aqua"}
tellraw @s "特殊地图需要特殊的解锁方式，需要地图制作者手动设定。"
tellraw @s "并且地图将放置于地图选择的末端，随机地图不会随机到特殊地图"
tellraw @s ["如果认为需要设定为特殊地图，请",{"text": "点击我","underlined": true,"clickEvent": {"action": "run_command","value": "/trigger mi_input set 1"}}]
tellraw @s ["如果认为需要撤回特殊地图的设定，请",{"text": "点击我","underlined": true,"clickEvent": {"action": "run_command","value": "/trigger mi_input set 2"}}]
tellraw @s ""
tellraw @s [{"text": "点击我","underlined": true,"clickEvent": {"action": "run_command","value": "/function debug:map_import/step_switch/220"}},{"text": "直接跳过这一步骤","underlined": false}]
tellraw @s ""