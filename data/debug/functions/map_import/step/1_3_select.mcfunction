# 选择地图实体

tellraw @s ""
tellraw @s {"text": ">> 步骤1_3：手动选择一个数据实体","bold": true,"color": "aqua"}
tellraw @s "数据实体存在一张地图的所有数据，而此程序是通过创造、添加和修改他的数据来导入地图"
tellraw @s ["如果是用/function map_importer/start来启动这个程序就可以直接跳过这一步。如果是/function map_importer/select来运行的话就请使用指令 ",{"text": "/trigger mi_input set <id>","underlined": true,"clickEvent": {"action": "suggest_command","value": "/trigger mi_input set "}},{"text": " 来设定！"}]
tellraw @s ["点击",{"text": "我","bold": false,"underlined": true,"clickEvent": {"action": "run_command","value": "/function debug:map_import/step_switch/210"}},"来跳过这一步..."]
tellraw @s ""