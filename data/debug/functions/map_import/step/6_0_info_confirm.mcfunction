# 信息确认

tellraw @s ""
tellraw @s {"text": ">> 步骤6_0：地图信息确认","bold": true,"color": "aqua"}
tellraw @s "大部分设定已经存入地图！现在请确认是否导入地图..."
tellraw @s "如果是的话则继续，如果取消则执行 /function minecraft:map_inporter/cancel"
tellraw @s ["如果完成输入，那么",{"text": "点击我","underlined": true,"clickEvent": {"action": "run_command","value": "/trigger mi_input"}},"来确认"]
tellraw @s ""