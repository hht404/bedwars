# 设定地图中文名称

give @s anvil{Tags:["map_importer","disable_throw"]}
give @s iron_nugget{Tags:["map_importer","disable_throw"]}

tellraw @s ""
tellraw @s {"text": ">> 步骤2_3：中文名称设定","bold": true,"color": "aqua"}
tellraw @s "好的地图总不能没有名称吧..."
tellraw @s "已经给予铁粒和铁砧放置到你的背包！请命名铁粒为地图的名称..."
tellraw @s ["命名完成后，拿在手上，然后",{"text": "点击我","underlined": true,"clickEvent": {"action": "run_command","value": "/trigger mi_input"}},"来确认名称..."]
tellraw @s ""