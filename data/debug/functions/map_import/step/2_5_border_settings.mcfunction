# 设定地图边界数据

tellraw @s ""
tellraw @s {"text": ">> 步骤2_5：地图边界数据","bold": true,"color": "aqua"}
tellraw @s "边界系统将防止玩家走出地图，设定合适的边界数据是必要的..."
tellraw @s ["请手动测量主岛的半径与地图半径。之后，分别用",{"text": "/trigger mi_border_min set <主岛半径>","underlined": true,"clickEvent": {"action": "suggest_command","value": "/trigger mi_border_min set "}}," 与 ",{"text": "/trigger mi_border set <地图半径>","underlined": true,"clickEvent": {"action": "suggest_command","value": "/trigger mi_border set "}}," 来告诉系统"]
tellraw @s ["如果完成输入，那么",{"text": "点击我","underlined": true,"clickEvent": {"action": "run_command","value": "/trigger mi_input"}},"来确认"]
tellraw @s ""