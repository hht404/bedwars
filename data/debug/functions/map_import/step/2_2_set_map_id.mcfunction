# 设定地图id

tellraw @s ""
tellraw @s {"text": ">> 步骤2_2：地图 ID 设定","bold": true,"color": "aqua"}
tellraw @s "地图 ID 是系统内部用来区分不同的地图而设定"
execute if data entity @e[type=marker,tag=map_importer,limit=1] data.map.id run tellraw @s ["系统已经自动填充了id（",{"nbt":"data.map.id","entity": "@e[type=marker,tag=map_importer,limit=1]"},"）因此你可以点击",{"text": "我","clickEvent": {"action": "run_command","value": "/function debug:map_import/step_switch/230"},"underlined": true},"来跳过这一步骤"]
execute unless data entity @e[type=marker,tag=map_importer,limit=1] data.map.id run tellraw @s [{"text": "在此程序中，请使用指令 "},{"text": "/trigger mi_input set <id>","underlined": true,"clickEvent": {"action": "suggest_command","value": "/trigger mi_input set "}},{"text": " 来设定！"}]
tellraw @s ""