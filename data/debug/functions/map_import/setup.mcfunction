# 地图导入程序 初始化

# 系统数据设定
gamerule doDaylightCycle false
gamerule sendCommandFeedback false
time set day
scoreboard players set $system_maintenance debug 1
forceload add 85 85 -85 -85 

# 创造临时计分板
scoreboard objectives add mi_mem dummy "载入程序变量"
scoreboard players set #init mi_mem 1 

scoreboard objectives add mi_menu trigger "地图导入菜单"
scoreboard objectives add mi_input trigger "程序交互接口"
scoreboard objectives add mi_border trigger "最大边界设定"
scoreboard objectives add mi_border_min trigger "最小边界设定"
scoreboard objectives add mi_kill trigger "清除标记"

# 挂载时钟
schedule function debug:map_import/tick2 2t replace

# 向全体玩家发送维护通知
tellraw @a [{"text": "[⚠] ","color": "yellow","bold": true},{"selector":"@s","bold": false},{"text": " 已载入地图导入程序，进入维护模式。","bold": false}]