# 导入新地图

# 玩家数据设定
team join debug @s
tag @s add map_importer

# 创造存储数据的实体并且设定id
kill @e[type=marker,tag=map_inporter]
execute summon marker at @s run function debug:map_import/entity_event/auto_map_id

# 开展主程序
function debug:map_import/setup

# 设定玩家步数
scoreboard players set @s mi_mem 110

# 发送主菜单
function debug:map_import/step/show