# 重置系统

# 清除正在进行的程序
schedule clear debug:map_import/tick2

# 加载必要强加载
forceload add 85 85 -85 -85 

# 清除玩家标签
tag @a remove map_importer

# 清除实体
kill @e[tag=map_importer]

# 清除计分板
scoreboard objectives remove mi_mem
scoreboard objectives remove mi_menu
scoreboard objectives remove mi_input
scoreboard objectives remove mi_border
scoreboard objectives remove mi_border_min
scoreboard objectives remove mi_kill

# 更新地图
execute store result score $map_max map if entity @e[type=marker,tag=map,tag=!special_map]
execute as @e[type=marker,tag=map] run scoreboard players operation $map_min map < @s map
execute if entity @e[type=marker,tag=map,tag=special_map] if score $map_min map matches 0 run scoreboard players remove $map_min map 1

# 取消维护模式
gamerule doDaylightCycle true
gamerule sendCommandFeedback true
scoreboard players set $system_maintenance debug 0