execute store result score $module_check temp run scoreboard players get #init mi_mem 

execute if score $module_check temp matches 1 run tellraw @s [{"text": "[⚠] ","color": "yellow","bold": true},{"text": "请注意！当前已经处于导入模式...","bold": false},{"text": "[我知道我在干什么]","bold": true,"clickEvent": {"action": "run_command","value": "/function debug:map_import/command/new_map"}}]
execute unless score $module_check temp matches 1 run function debug:map_import/command/new_map

scoreboard players reset $module_check temp