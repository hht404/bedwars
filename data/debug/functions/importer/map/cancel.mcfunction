execute store result score $module_check temp run scoreboard players get #init mi_mem 

execute if score $module_check temp matches 1 run tellraw @s [{"text": "[⚠] ","color": "yellow","bold": true},{"text": "请注意！执行取消操作将直接清除当前编辑的实体... ","bold": false},{"text": "[我知道我在干什么]","bold": true,"clickEvent": {"action": "run_command","value": "/function debug:map_import/command/cancel"}}]
execute unless score $module_check temp matches 1 run tellraw @s [{"text": "[⚠] ","color": "red","bold": true},{"text": "程序并没有加载！","bold": false}]
execute unless score $module_check temp matches 1 run function lib:sounds/error

scoreboard players reset $module_check temp