# 管理员调试面板

# 系统：重置游戏 开始游戏 测试游戏 调试面板 维护模式 结束游戏
# 队伍：自动分配 平均分配 复活床/摧毁床 高亮出生点
# 规则：资源点冷却时间 瞬间复活/等待时间 重复击杀惩罚 虚空伤害 游戏倒计时 边界缩圈 昼夜交替
# 追踪：伤害追踪 死亡信息 射线路径 
# 
# 地图编辑/(SHIFT)锁定：[地图名称]


# 标题
tellraw @s [{"text":"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n ❇  ","color":"aqua"},{"text": "方块竞技场","strikethrough": true},{"text":"起床战争控制面板\n"},{"text":"     妈耶，难不成这就是传说中神的力量？\n","color":"gray"}]

# 系统
summon marker ~ ~ ~ {CustomName:'{"text":"[重置游戏]","color":"white"}',Tags:["debug1"]}
execute unless score $working gaming matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[开始游戏]","color":"white"}',Tags:["debug2"]}
execute if score $working gaming matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[结束游戏]","color":"white"}',Tags:["debug2"]}
summon marker ~ ~ ~ {CustomName:'{"text":"[测试游戏]","color":"white"}',Tags:["debug3"]}
execute if score $system_f3 debug matches 0 run summon marker ~ ~ ~ {CustomName:'{"text":"[F3面板]","color":"gray"}',Tags:["debug4"]}
execute if score $system_f3 debug matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[F3面板]","color":"white"}',Tags:["debug4"]}
execute if score $system_maintenance debug matches 0 run summon marker ~ ~ ~ {CustomName:'{"text":"[维护模式]","color":"gray"}',Tags:["debug5"]}
execute if score $system_maintenance debug matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[维护模式]","color":"white"}',Tags:["debug5"]}
tellraw @s ["     系统  >>   ",{"selector":"@e[type=marker,tag=debug1]","clickEvent":{"action":"run_command","value":"/function debug:init"},"hoverEvent": {"action": "show_text","value": "将所有系统及模块进行初始化"}},"  ",{"selector":"@e[type=marker,tag=debug2]","clickEvent":{"action":"run_command","value":"/function debug:panel_trigger/system_game_state"},"hoverEvent": {"action": "show_text","value": "将所有非 debug 玩家强制参与游戏或者强制结束游戏"}},"  ",{"selector":"@e[type=marker,tag=debug3]","clickEvent":{"action":"run_command","value":"/function debug:test_game"},"hoverEvent": {"action": "show_text","value": "将所有非 debug 玩家强制参与游戏并且游戏不再自动结束"}},"  ",{"selector":"@e[type=marker,tag=debug4]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/system_f3"},"hoverEvent": {"action": "show_text","value": "启用/禁用 原版调试界面"}},"  ",{"selector":"@e[type=marker,tag=debug5]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/system_maintenance"},"hoverEvent": {"action": "show_text","value": "开启/关闭 服务器维护模式"}}]
kill @e[type=marker,tag=debug1]
kill @e[type=marker,tag=debug2]
kill @e[type=marker,tag=debug3]
kill @e[type=marker,tag=debug4]
kill @e[type=marker,tag=debug5]

# 队伍：平均分配 复活床/摧毁床 高亮出生点
execute if score $team_auto_allocation debug matches 0 run summon marker ~ ~ ~ {CustomName:'{"text":"[自动分配]","color":"white"}',Tags:["debug1"]}
execute if score $team_auto_allocation debug matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[自动分配]","color":"gray"}',Tags:["debug1"]}
tellraw @s ["     队伍  >>   ",{"selector":"@e[type=marker,tag=debug1]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/team_auto_allocation"},"hoverEvent": {"action": "show_text","value": "允许/禁止 未参与玩家在游戏开始后自动加入游戏"}}]
kill @e[type=marker,tag=debug1]

# 规则：重复击杀惩罚
execute if score $rule_res_cd debug matches 0 run summon marker ~ ~ ~ {CustomName:'{"text":"[资源点冷却 等待冷却]","color":"gray"}',Tags:["debug1"]}
execute if score $rule_res_cd debug matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[资源点冷却 立即生成]","color":"yellow"}',Tags:["debug1"]}
execute if score $respawn_wait gamerule matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[复活时间 等待时间]","color":"white"}',Tags:["debug2"]}
execute if score $respawn_wait gamerule matches 2 run summon marker ~ ~ ~ {CustomName:'{"text":"[复活时间 立即复活]","color":"white"}',Tags:["debug2"]}
execute if score $void_damage gamerule matches 0 run summon marker ~ ~ ~ {CustomName:'{"text":"[虚空伤害 原版伤害]","color":"white"}',Tags:["debug3"]}
execute if score $void_damage gamerule matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[虚空伤害 添加伤害]","color":"white"}',Tags:["debug3"]}
execute if score $void_damage gamerule matches 2 run summon marker ~ ~ ~ {CustomName:'{"text":"[虚空伤害 立即死亡]","color":"white"}',Tags:["debug3"]}
execute if score $border gamerule matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[边界缩圈 不会缩圈]","color":"white"}',Tags:["debug4"]}
execute if score $border gamerule matches 2 run summon marker ~ ~ ~ {CustomName:'{"text":"[边界缩圈 自动缩圈]","color":"white"}',Tags:["debug4"]}
execute if score $daylight_cycle gamerule matches 1 run summon marker ~ ~ ~ {CustomName:'{"text":"[昼夜更替 启用]","color":"white"}',Tags:["debug5"]}
execute if score $daylight_cycle gamerule matches 2 run summon marker ~ ~ ~ {CustomName:'{"text":"[昼夜更替 停用]","color":"white"}',Tags:["debug5"]}

tellraw @s ["     规则  >>   ",{"selector":"@e[type=marker,tag=debug1]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/rule_res_cd"},"hoverEvent": {"action": "show_text","value": "资源点是否可以有设定的冷却时间并且同步所有资源点时间（如果设定为“无”则代表资源点将立即生成，没有冷却时间）（注意：使用 config 计分板来恢复资源点冷却时间）"}},"  ",{"selector":"@e[type=marker,tag=debug2]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/rule_respawn_wait"},"hoverEvent": {"action": "show_text","value": "开启/停用 复活时等待时间（玩家可调）"}},"  ",{"selector":"@e[type=marker,tag=debug3]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/rule_void_damage"},"hoverEvent": {"action": "show_text","value": "设定虚空伤害（玩家可调）为：原版/额外/致死"}},"  ",{"selector":"@e[type=marker,tag=debug4]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/rule_border"},"hoverEvent": {"action": "show_text","value": "允许/停用 边界缩圈（玩家可调）"}},"  ",{"selector":"@e[type=marker,tag=debug5]","clickEvent": {"action": "run_command","value": "/function debug:panel_trigger/rule_daylight_cycle"},"hoverEvent": {"action": "show_text","value": "启用/停止 昼夜更替（玩家可调）"}}]
kill @e[type=marker,tag=debug1]
kill @e[type=marker,tag=debug2]
kill @e[type=marker,tag=debug3]
kill @e[type=marker,tag=debug4]
kill @e[type=marker,tag=debug5]

# 末尾
tellraw @s ""

# 发送点击音效
function lib:sounds/hit