# 每 2 Tick 执行一次
# as / at @a[tag=shop_importer]

# 发送问题警告（缺少商店绑定）
execute as @e[tag=shop,type=marker] if score @s shop = @p shop run scoreboard players set @p si_mem 1
execute unless score @s si_mem matches 1 run title @p actionbar [{"text": "⚠ 减少实体编辑中 使用 ","color": "yellow"},{"text": "/trigger si_id set <商店 ID >","underlined": true},{"text": " 来更新需要更改的商店"}]

# 设定 ID
execute if score @s si_id matches 1.. run function debug:shop_import/trigger/set_id

# 手动同步
execute if score @s si_sync_trigger matches 1.. run function debug:shop_import/trigger/sync

# 填充背包
# item replace entity @s hotbar.0 w

# 允许触发触发项
scoreboard players enable @s si_id
scoreboard players enable @s si_sync_trigger