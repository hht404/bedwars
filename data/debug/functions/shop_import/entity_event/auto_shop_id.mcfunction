# 设定商店id

# 更新 Tags
data modify entity @s Tags set value ["shop","shop_importer","shop_temp"]

# 更新位置
tp @s 0 0 0 

# 设定 ID
execute store result score @s shop run scoreboard players get $shop_max mem 
scoreboard players operation $shop_max mem += $1 const

execute store result entity @s data.shop.id double 1 run scoreboard players get @s shop