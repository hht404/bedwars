# 同步实体数据

# 重置间隙
scoreboard players set @s si_sync 30

# 检测环境并选择实体
tag @e[type=marker,tag=shop_editing] remove shop_editing
execute as @e[tag=shop,type=marker] if score @s shop = @p shop run tag @s add shop_editing
execute unless entity @e[type=marker,tag=shop,tag=shop_editing] run tellraw @s [{"text": "[⚠] ","color": "red","bold": true},{"text": "没有检测到实体绑定，同步实体数据任务中断！","bold": false}]
execute unless entity @e[type=marker,tag=shop,tag=shop_editing] run function lib:sounds/error
execute unless entity @e[type=marker,tag=shop,tag=shop_editing] run return 1

# 替换数据
## 同步数据
data modify entity @e[type=marker,tag=shop_editing,limit=1] data.shop.Items set from entity @s Inventory
## 移除快捷栏
# data remove entity @e[type=marker,tag=shop_editing,limit=1] data.shop.Items[{S}]

# 发送提示
title @s actionbar "完成实体数据同步..."