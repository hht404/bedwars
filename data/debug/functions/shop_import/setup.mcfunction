# 商店导入程序 初始化

# 创造临时计分板
scoreboard objectives add si_mem dummy "载入程序变量"
scoreboard players set #init si_mem 1 

scoreboard objectives add si_id trigger "商店 ID 设定"
scoreboard objectives add si_sync trigger "商店同步间隔"
scoreboard objectives add si_sync_trigger trigger "商店手动同步"

# 挂载时钟
schedule function debug:shop_import/tick2 2t replace
schedule function debug:shop_import/tick20 20t replace