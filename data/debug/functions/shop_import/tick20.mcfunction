# 每 20 Tick 执行一次
schedule function debug:shop_import/tick20 20t replace

# 自动同步倒计时
execute as @a[scores={si_sync=1..},tag=shop_importer] at @s as @e[type=marker,tag=shop] if score @s shop = @p shop run scoreboard players remove @p si_sync 1
execute as @a[scores={si_sync=0}] at @s run function debug:shop_import/entity_event/sync