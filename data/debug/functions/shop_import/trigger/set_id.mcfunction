# 设定编辑实体的 ID

# 尝试存储商店数据
function debug:shop_import/entity_event/sync

# 同步 ID
scoreboard players operation @s shop = @s si_id
scoreboard players reset @s si_id

# 发送通知
title @s actionbar ["当前编辑商店 ID 设定为 ",{"score":{"objective": "shop","name": "@s"}}]