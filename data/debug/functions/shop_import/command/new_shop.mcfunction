# 新商店编辑

# 玩家数据设定
team join debug @s
tag @s add shop_importer

# 创造存储实体并且设定id
execute summon marker at @s run function debug:shop_import/entity_event/auto_shop_id

# 绑定玩家 ID
scoreboard players operation @s shop = @e[type=marker,tag=shop,tag=shop_temp] shop
tag @e[type=marker,tag=shop_temp,tag=shop] remove shop_temp

# 开展主程序
function debug:shop_import/setup

# 发送教程
tellraw @s ""
tellraw @s {"text": ">> 知道导入程序如何使用","bold": true,"color": "aqua"}
tellraw @s "导入程序分为两部分：商店内容和物品设定"
tellraw @s "背包中的前三行分别对应商店的前三行！"
tellraw @s "物品设定就在快捷栏中，特别分了前中后3个功能区域。"
tellraw @s "第一格代表操作物品。中间代表此物品在商店中的价格，对应的颜色对应四个基础资源..."
tellraw @s "最后是应用和撤销操作了，顾名思义。我也不必多讲了！"
tellraw @s ["操作会每30秒进行一次同步，如果想立即同步请使用 ",{"text": "/trigger si_sync","clickEvent": {"action": "run_command","value": "/trigger si_sync_trigger"}}," ！"]