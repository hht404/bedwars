# 全局初始化

# 显示提示
tellraw @a ["",{"text": "[❈] ","color": "aqua","bold": true}, {"text": "管理员已手动重置游戏!","color": "aqua"}]

# 游戏规则
gamerule commandModificationBlockLimit 9834110
gamerule disableElytraMovementCheck true
gamerule doImmediateRespawn true
gamerule keepInventory false
gamerule doLimitedCrafting true
gamerule fallDamage true
gamerule naturalRegeneration true
gamerule doDaylightCycle true
gamerule doFireTick true

# 清理游戏区域和其他改动区域
fill 0 319 0 0 318 0 air
fill -149 -64 -149 149 45 149 air 
fill -100 43 140 101 -64 160 air
kill @e[type=item]

# 清除实体
function bw:global/clear_entity
kill @e[tag=gamerule]

# 清除计划任务
schedule clear bw:lobby/map/init/clone/marker_clone

# 移除容器 NBT
data remove storage bw:map name
data remove storage bw:map team
data remove storage bw:map id
data remove storage bw:map spawner
data remove storage bw:map border
data remove storage bw:map center
data remove storage temp player_kill_streak
# data remove storage temp version

# 玩家数据控制
effect clear @a 
recipe take @a *
tag @a remove playing
tag @a remove afk_check

# 调用全局初始化
function bw:global/init

# 调用大厅初始化
function bw:lobby/init

# 调用游戏初始化
function bw:game/init

# HUB 控制
scoreboard objectives setdisplay belowName
scoreboard objectives setdisplay list
scoreboard objectives setdisplay sidebar

# 挂载系统时钟
schedule function bw:global/clock/2tick 2t replace
schedule function bw:global/clock/10tick 10t replace
schedule function bw:global/clock/15tick 15t replace
schedule function bw:global/clock/20tick 20t replace

# 载入大厅状态
function bw:lobby/setup