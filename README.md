# Minecraft BedWars Datapack · ![](https://img.shields.io/badge/minecraft-1.20.1-green.svg)

这是一个由 Minecraft 原生语言 mcfunction 制成的 BedWars 系统数据包。

玩家们在游戏中竞技，摧毁对方的床以阻止他们重生，并努力成为最后的赢家..

## 项目特征

- **起床战争游戏模式**: 玩家们可以组队或者单人参与，允许只有两个玩家时启动游戏。
- **资源获取**：资源点添加冷却和最多产出量，前者限制玩家发育速度，后者不会让资源点出现堆积如山的情况。
- **策略和团队合作**: 此数据包强调策略和团队合作，装备压制、玩家陷阱可能更常出现。
- **管理员友善**：数据包添加傻瓜式地图导入程序和简单易用的管理员面板用于后期维护。

## 如何使用

注意：请勿直接下载，管理员将不定期发布整张地图作为发行版本供作下载。

1. 下载适用您当前 Minecraft 版本的最新发行版本。
2. 解压并且将存档放入 saves 文件夹中，服务器方面需要更名为 world 并且放入服务器文件夹。
3. 邀请好友一起加入游戏...

## 一起开发

欢迎对本数据包提出改进建议、报告问题或贡献代码。如果你想参与贡献，请遵循以下步骤：

1. Fork 本项目。
2. 创造一个新的分支: `git checkout -b feature/你的特性` 或 `bugfix/你的修复`。
3. 对仓库进行修改或添加新特性。
4. 提交你的更改：`git commit -m '添加了一个...'`。
5. 将你的更改推送到远程分支：`git push origin 你的分支名`
6. 提交一个 PULL 请求

## 一起游玩（服务器）
此项目将不定时推送更新到服务器 git 仓库，保证代码的最新性。最新的内容将广播到服务器中。

你可以加入服务器一起同我们游玩！这是我们的 QQ 交流群：817621853

## 授权条框

本项目部分内容参考龙腾猫跃制作组发行的[游艺街](https://www.bilibili.com/read/cv21354706)和[方块竞技场](https://www.bilibili.com/read/cv15066819)，因此遵循上游项目的授权条款：

- 你可以发布游戏的图片、视频、直播，如果能附上本文的链接就更好了！
- 你可以用它开服，或是作为服务器的一部分。
- 你可以将它搬运到 B 站和 MCBBS 以外的地方，但必须包含制作人员列表和本文的链接。
- 你可以随意修改它，但不能让任何非装饰性内容可以通过付费手段获取。例如，你可以添加付费的头衔、特效，但不能添加付费的攻击力提升、额外物品等。
- 你可以公开修改后的版本，或是制作衍生作品，但必须继续使用本授权条款（可以附加额外限制，但不能删除其中的条目），且在适当位置给出制作人员列表和本文的链接。

**免责声明：** 本软件是玩家自制的非官方内容，可能会与官方游戏机制存在不同。在使用本项目时，请确保备份存档以防止意外丢失。

更多有关Minecraft的信息，请访问 [Minecraft官方网站](https://www.minecraft.net/)。